package com.wow.alcom.ooredooCard;


import android.content.Context;
import android.util.Log;
import android.widget.ImageView;
import android.widget.Toast;

import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ServiceManager {

    public static APIInterface getInterface() {
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        return apiInterface;
    }

    public static final String TAG = "";

    public interface ResponseHandler {
        public void onSuccess(Object response, int responseCode);

        public void onError();

        public void onError(String message);
    }

    public interface MultipartResponseHandler {
        public void onUpdateProgress(long bytesUploaded, long totalBytes);

        public void onSuccess(Object response);

        public void onError(Object error);
    }



    public static void request(Call<ResponseBody> jsonObject, final Context context, final ResponseHandler responseHandler) {

        /*if (Util.isNetworkAvailable(context))*/ {
            jsonObject.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 203 || response.code() == 500 || response.code() == 400) {
                        try {
                            String resp = response.body().string();
                            JSONObject jsonObject1 = new JSONObject(resp);

                            if (call.request().url().url().toString().contains("background-check") && jsonObject1.has("data")) {
                                Log.d(ServiceManager.class.getSimpleName(), "onResponse: INside if client token present");
                                responseHandler.onError(resp);
                            } else {
                                String message = jsonObject1.getString("msg");
                                responseHandler.onError(message);
                            }
                            return;
                        } catch (Exception e) {
                            e.printStackTrace();
                            responseHandler.onError(response.message());
                        }

                    } else if (response.code() == 401) {
                        responseHandler.onError("Unauthorised");
                        return;
                    } else if (response.code() == 406) {
                        try {
                            responseHandler.onSuccess(response.errorBody().string(), response.code());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        return;
                    }
                    if (response.code() == 200 || response.code() == 206 || response.code() == 201) {
                        try {
                            responseHandler.onSuccess(response.body().string(), response.code());
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    responseHandler.onError("No Internet Connection");
                    Toast.makeText(context.getApplicationContext(), t.getLocalizedMessage().toString(), Toast.LENGTH_LONG).show();
                }
            });
        } /*else responseHandler.onError(context.getString(R.string.no_internet));*/

    }


    public static void requestWithMultipartPost(HashMap<String, String> stringHashMap, File imageFile, String path, boolean isHUD, final MultipartResponseHandler responseHandler) {


    }

    public static void cancelRequest(String url) {

    }

    public static void cancleAllRequest() {

    }

    public static void loadImage(final ImageView imageView, String imageUrl, int width, int height) {

    }
}