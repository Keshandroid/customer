package com.wow.alcom.ooredooCard.model;

public class APIResp {
    public String result;
    public int responsecode;
    public int authcode;
    public Object RRN;
    public Object ECI;
    public double tranid;
    public String trackid;
    public String terminalid;
    public Object udf1;
    public Object udf2;
    public Object udf3;
    public Object udf4;
    public String udf5;

    @Override
    public String toString() {
        return "Response{" +
                "result='" + result + '\'' +
                ", responsecode=" + responsecode +
                ", authcode=" + authcode +
                ", RRN=" + RRN +
                ", ECI=" + ECI +
                ", tranid=" + tranid +
                ", trackid='" + trackid + '\'' +
                ", terminalid='" + terminalid + '\'' +
                ", udf1=" + udf1 +
                ", udf2=" + udf2 +
                ", udf3=" + udf3 +
                ", udf4=" + udf4 +
                ", udf5='" + udf5 + '\'' +
                '}';
    }
}

