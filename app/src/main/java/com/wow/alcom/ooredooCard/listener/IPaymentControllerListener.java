package com.wow.alcom.ooredooCard.listener;

public interface IPaymentControllerListener {

    void callPaymentApi(String request);
    void callMobilePaymentApi(String request);
    void callRefundPaymentApi(String request);
}
