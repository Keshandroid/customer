package com.wow.alcom.ooredooCard.model;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentResp implements Parcelable {

    @SerializedName("response")
    @Expose
    private Response response;
    public final static Creator<PaymentResp> CREATOR = new Creator<PaymentResp>() {


        @SuppressWarnings({
                "unchecked"
        })
        public PaymentResp createFromParcel(android.os.Parcel in) {
            return new PaymentResp(in);
        }

        public PaymentResp[] newArray(int size) {
            return (new PaymentResp[size]);
        }

    };

    protected PaymentResp(android.os.Parcel in) {
        this.response = ((Response) in.readValue((Response.class.getClassLoader())));
    }

    public PaymentResp() {
    }

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(response);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "PaymentResp{" +
                "response=" + response +
                '}';
    }
}