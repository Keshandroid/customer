package com.wow.alcom.ooredooCard;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.util.Log;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

import com.wow.alcom.R;
import com.wow.alcom.activity.TimeSlotActivity;

public class ReceiptWebViewActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_receipt_web_view);

        Intent intentData = getIntent();
        String url = intentData.getStringExtra("url");
        Log.d("ReceiptWebViewActivity", "onCreate: load url : " + url);

        WebView mWebView = (WebView) findViewById(R.id.webview);
        // Enable Javascript
        WebSettings webSettings = mWebView.getSettings();
        webSettings.setJavaScriptEnabled(true);

        mWebView.setWebViewClient(new MyWebViewClient());
        webSettings.setDomStorageEnabled(true);
        if (!isFinishing())
            showProgress();
        mWebView.loadUrl(url);
    }

    private class MyWebViewClient extends WebViewClient {
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            showProgress();
            String response = url.substring(url.indexOf("?") + 1, url.length());
            Log.d("ReceiptWebViewActivity", "onPageStarted url : " + url);
            String[] values = response.split("&");
            if (response.contains("tranid")) {
                Intent i = new Intent(ReceiptWebViewActivity.this, TimeSlotActivity.class);
                for (String pair : values) {
                    String[] nameValue = pair.split("=");
                    if (nameValue.length == 2) {
                        System.out.println("ReceiptWebViewActivity: Name:" + nameValue[0] + " value:" + nameValue[1]);
                        i.putExtra(nameValue[0], nameValue[1]);
                    }
                }
                i.putExtra("receipt_url", url);
                setResult(RESULT_OK, i);
                finish();
            }
            super.onPageStarted(view, url, favicon);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            dismissProgress();
        }
    }
}