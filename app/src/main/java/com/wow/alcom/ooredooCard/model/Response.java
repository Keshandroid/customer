package com.wow.alcom.ooredooCard.model;

import android.os.Parcelable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Response implements Parcelable {

    @SerializedName("tranid")
    @Expose
    private String tranid;
    @SerializedName("responsecode")
    @Expose
    private String responsecode;
    @SerializedName("trackid")
    @Expose
    private String trackid;
    @SerializedName("udf5")
    @Expose
    private String udf5;
    @SerializedName("ECI")
    @Expose
    private String eci;
    @SerializedName("udf3")
    @Expose
    private String udf3;
    @SerializedName("terminalid")
    @Expose
    private String terminalid;
    @SerializedName("udf4")
    @Expose
    private String udf4;
    @SerializedName("udf1")
    @Expose
    private String udf1;
    @SerializedName("udf2")
    @Expose
    private String udf2;
    @SerializedName("RRN")
    @Expose
    private String rrn;
    @SerializedName("result")
    @Expose
    private String result;
    @SerializedName("authcode")
    @Expose
    private String authcode;
    @SerializedName("payId")
    @Expose
    private String payId;
    @SerializedName("targetUrl")
    @Expose
    private String targetUrl;
    public final static Creator<Response> CREATOR = new Creator<Response>() {


        @SuppressWarnings({
                "unchecked"
        })
        public Response createFromParcel(android.os.Parcel in) {
            return new Response(in);
        }

        public Response[] newArray(int size) {
            return (new Response[size]);
        }

    };

    protected Response(android.os.Parcel in) {
        this.tranid = ((String) in.readValue((String.class.getClassLoader())));
        this.responsecode = ((String) in.readValue((String.class.getClassLoader())));
        this.trackid = ((String) in.readValue((String.class.getClassLoader())));
        this.udf5 = ((String) in.readValue((String.class.getClassLoader())));
        this.eci = ((String) in.readValue((String.class.getClassLoader())));
        this.udf3 = ((String) in.readValue((String.class.getClassLoader())));
        this.terminalid = ((String) in.readValue((String.class.getClassLoader())));
        this.udf4 = ((String) in.readValue((String.class.getClassLoader())));
        this.udf1 = ((String) in.readValue((String.class.getClassLoader())));
        this.udf2 = ((String) in.readValue((String.class.getClassLoader())));
        this.rrn = ((String) in.readValue((String.class.getClassLoader())));
        this.result = ((String) in.readValue((String.class.getClassLoader())));
        this.authcode = ((String) in.readValue((String.class.getClassLoader())));
    }

    public Response() {
    }

    public String getTranid() {
        return tranid;
    }

    public void setTranid(String tranid) {
        this.tranid = tranid;
    }

    public String getResponsecode() {
        return responsecode;
    }

    public void setResponsecode(String responsecode) {
        this.responsecode = responsecode;
    }

    public String getTrackid() {
        return trackid;
    }

    public void setTrackid(String trackid) {
        this.trackid = trackid;
    }

    public String getUdf5() {
        return udf5;
    }

    public void setUdf5(String udf5) {
        this.udf5 = udf5;
    }

    public String getEci() {
        return eci;
    }

    public void setEci(String eci) {
        this.eci = eci;
    }

    public String getUdf3() {
        return udf3;
    }

    public void setUdf3(String udf3) {
        this.udf3 = udf3;
    }

    public String getTerminalid() {
        return terminalid;
    }

    public void setTerminalid(String terminalid) {
        this.terminalid = terminalid;
    }

    public String getUdf4() {
        return udf4;
    }

    public void setUdf4(String udf4) {
        this.udf4 = udf4;
    }

    public String getUdf1() {
        return udf1;
    }

    public void setUdf1(String udf1) {
        this.udf1 = udf1;
    }

    public String getUdf2() {
        return udf2;
    }

    public void setUdf2(String udf2) {
        this.udf2 = udf2;
    }

    public String getRrn() {
        return rrn;
    }

    public void setRrn(String rrn) {
        this.rrn = rrn;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getAuthcode() {
        return authcode;
    }

    public void setAuthcode(String authcode) {
        this.authcode = authcode;
    }

    public String getPayId() {
        return payId;
    }

    public void setPayId(String payId) {
        this.payId = payId;
    }

    public String getTargetUrl() {
        return targetUrl;
    }

    public void setTargetUrl(String targetUrl) {
        this.targetUrl = targetUrl;
    }

    public void writeToParcel(android.os.Parcel dest, int flags) {
        dest.writeValue(tranid);
        dest.writeValue(responsecode);
        dest.writeValue(trackid);
        dest.writeValue(udf5);
        dest.writeValue(eci);
        dest.writeValue(udf3);
        dest.writeValue(terminalid);
        dest.writeValue(udf4);
        dest.writeValue(udf1);
        dest.writeValue(udf2);
        dest.writeValue(rrn);
        dest.writeValue(result);
        dest.writeValue(authcode);
    }

    public int describeContents() {
        return 0;
    }

    @Override
    public String toString() {
        return "Response{" +
                "tranid='" + tranid + '\'' +
                ", responsecode='" + responsecode + '\'' +
                ", trackid='" + trackid + '\'' +
                ", udf5='" + udf5 + '\'' +
                ", eci='" + eci + '\'' +
                ", udf3='" + udf3 + '\'' +
                ", terminalid='" + terminalid + '\'' +
                ", udf4='" + udf4 + '\'' +
                ", udf1='" + udf1 + '\'' +
                ", udf2='" + udf2 + '\'' +
                ", rrn='" + rrn + '\'' +
                ", result='" + result + '\'' +
                ", authcode='" + authcode + '\'' +
                ", payId='" + payId + '\'' +
                ", targetUrl='" + targetUrl + '\'' +
                '}';
    }
}