package com.wow.alcom.ooredooCard;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.wow.alcom.activity.BookingActivity;
import com.wow.alcom.ooredooCard.listener.IPaymentControllerListener;
import com.wow.alcom.ooredooCard.model.PaymentResp;
import com.wow.alcom.ooredooCard.view.IPaymentView;

import org.json.JSONObject;
import fr.arnaudguyon.xmltojsonlib.XmlToJson;

import java.io.Reader;
import java.io.StringReader;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;

public class PaymentController implements IPaymentControllerListener {
    private final String TAG = "PaymentController";
    private IPaymentView view1;
    private Context context;

    public PaymentController(Context context, IPaymentView view) {
        this.context = context;
        this.view1 = view;
    }

    @Override
    public void callPaymentApi(String request) {
        view1.showProgress();
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/xml"), request);
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> call = apiInterface.payment(requestBody);
        ServiceManager.request(call, context, new ServiceManager.ResponseHandler() {
            @Override
            public void onSuccess(Object response, int responseCode) {
                view1.dismissProgress();
                Log.d(TAG, "onResponse: == " + response.toString());
                String xmlString = response.toString();
                XmlToJson xmlToJson = new XmlToJson.Builder(xmlString).build();
                JSONObject jsonObject = xmlToJson.toJson();

                // convert to a Json String
                String jsonString = xmlToJson.toString();

                // convert to a formatted Json String
                String formatted = xmlToJson.toFormattedString();
                Reader reader = new StringReader(jsonObject.toString());
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();

                PaymentResp responseData = gson.fromJson(reader, PaymentResp.class);
                Log.d(TAG, "onSuccess: " + responseData);
                view1.onSuccess(responseData);

            }

            @Override
            public void onError() {
                view1.dismissProgress();
            }

            @Override
            public void onError(String message) {
                view1.dismissProgress();
            }
        });
    }

    @Override
    public void callMobilePaymentApi(String request) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/xml"), request);
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> call = apiInterface.mobilePayment(requestBody);
        ServiceManager.request(call, context, new ServiceManager.ResponseHandler() {
            @Override
            public void onSuccess(Object response, int responseCode) {
                view1.dismissProgress();
                Log.d(TAG, "onResponse: == " + response.toString());
                String xmlString = response.toString();
                XmlToJson xmlToJson = new XmlToJson.Builder(xmlString).build();
                JSONObject jsonObject = xmlToJson.toJson();
                Log.d(TAG, "onResponse2: == " + jsonObject);

                // convert to a Json String
                String jsonString = xmlToJson.toString();

                // convert to a formatted Json String
                String formatted = xmlToJson.toFormattedString();
                Reader reader = new StringReader(jsonObject.toString());
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();

                PaymentResp responseData = gson.fromJson(reader, PaymentResp.class);
                Log.d(TAG, "onSuccess: " + responseData);
                view1.onSuccess(responseData);

            }

            @Override
            public void onError() {
                view1.dismissProgress();
            }

            @Override
            public void onError(String message) {
                view1.dismissProgress();
            }
        });
    }

    @Override
    public void callRefundPaymentApi(String request) {
        RequestBody requestBody = RequestBody.create(MediaType.parse("application/xml"), request);
        APIInterface apiInterface = APIClient.getClient().create(APIInterface.class);
        Call<ResponseBody> call = apiInterface.mobilePayment(requestBody);
        ServiceManager.request(call, context, new ServiceManager.ResponseHandler() {
            @Override
            public void onSuccess(Object response, int responseCode) {
                view1.dismissProgress();
                Log.d(TAG, "onResponse: == " + response.toString());
                String xmlString = response.toString();
                XmlToJson xmlToJson = new XmlToJson.Builder(xmlString).build();
                JSONObject jsonObject = xmlToJson.toJson();
                Log.d(TAG, "onResponse2: == " + jsonObject);

                // convert to a Json String
                String jsonString = xmlToJson.toString();

                // convert to a formatted Json String
                String formatted = xmlToJson.toFormattedString();
                Reader reader = new StringReader(jsonObject.toString());
                GsonBuilder builder = new GsonBuilder();
                Gson gson = builder.create();

                PaymentResp responseData = gson.fromJson(reader, PaymentResp.class);
                Log.d(TAG, "onSuccess: " + responseData);
                view1.onSuccess(responseData);

            }

            @Override
            public void onError() {
                view1.dismissProgress();
            }

            @Override
            public void onError(String message) {
                view1.dismissProgress();
            }
        });
    }
}
