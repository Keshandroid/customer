package com.wow.alcom.ooredooCard;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.RequestBody;
import okhttp3.logging.HttpLoggingInterceptor;
import okio.Buffer;
import retrofit2.Retrofit;
import retrofit2.converter.simplexml.SimpleXmlConverterFactory;

public class APIClient {

    private static Retrofit retrofit = null;
    //private static String kBaseURL="https://test.vistamoney.info/paymentgateway/payments/";
    private static String kBaseURL="https://vistamoney.info/paymentgateway/payments/";

    public static Retrofit getClient() {

        final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);

        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).readTimeout(80, TimeUnit.SECONDS).connectTimeout(80, TimeUnit.SECONDS).build();

//        OkHttpClient okHttpClient = new OkHttpClient.Builder().addInterceptor(new Interceptor() {
//            @Override
//            public Response intercept(Chain chain) throws IOException {
//                Request request = chain.request();
//                Request.Builder requestBuilder = request.newBuilder();
//                String roleID = "", id = "";
//
//                if (User.getCurrentUser() != null){
//                    roleID = User.getCurrentUser().getRoleId();
//                    id= User.getCurrentUser().getId();
//                }
//                RequestBody formBody = new FormBody.Builder()
//                        .add("device_OS", "ANDROID")
//                        .add("device_token", "1234567890")
//                        .add("role_id", roleID)
//                        .add("user_id", id)
//                        .build();
//
//                String postBodyString = APIClient.bodyToString(request.body());
//                postBodyString += ((postBodyString.length() > 0) ? "&" : "") +  APIClient.bodyToString(formBody);
//                request = requestBuilder
//                        .post(RequestBody.create(MediaType.parse("application/json;charset=UTF-8"), postBodyString))
//                        .build();
//
//                return chain.proceed(request);
//            }
//        }).addInterceptor(interceptor).build();

        retrofit = new Retrofit.Builder()
                .baseUrl(kBaseURL)
                .addConverterFactory(SimpleXmlConverterFactory.create())
                .client(client)
                .build();

        return retrofit;
    }


    public static String bodyToString(final RequestBody request){
        try {
            final RequestBody copy = request;
            final Buffer buffer = new Buffer();
            if(copy != null)
                copy.writeTo(buffer);
            else
                return "";
            return buffer.readUtf8();
        }
        catch (final IOException e) {
            return "did not work";
        }
    }
}
