package com.wow.alcom.ooredooCard.view;

public interface IBaseView {
    void showProgress();
    void dismissProgress();
}
