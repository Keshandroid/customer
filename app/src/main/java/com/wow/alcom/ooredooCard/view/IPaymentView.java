package com.wow.alcom.ooredooCard.view;

import com.wow.alcom.ooredooCard.model.PaymentResp;
public interface IPaymentView extends IBaseView {
    void onSuccess(PaymentResp paymentResp);

    void onFailed(String message);
}
