package com.wow.alcom.ooredooCard;


import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.POST;

public interface APIInterface {

    @POST("performXmlTransaction")
    Call<ResponseBody> payment(@Body RequestBody body);

    @POST("mobilePaymentXml")
    Call<ResponseBody> mobilePayment(@Body RequestBody body);
}