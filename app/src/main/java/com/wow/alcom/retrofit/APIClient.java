package com.wow.alcom.retrofit;


import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class APIClient {
    static Retrofit retrofit = null;

    //https://keshavinfotechdemo1.com/keshav/KG2/WOW_Project/wow/tcapi/s_home_data.php
    //https://wow.wow2all.com/tcapi/s_home_data.php

    public static String baseUrl = "https://wow.wow2all.com";
    //public static String baseUrl = "http://keshavinfotechdemo1.com/keshav/KG2/WOW_Project";
    //public static String imageBaseUrl = "http://keshavinfotechdemo1.com/keshav/KG2/WOW_Project/wow";
    public static String imageBaseUrl = "https://wow.wow2all.com";

    public static final String APPEND_URL = "/tcapi/";
    public static UserService getInterface() {
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(interceptor)
                .build();
        retrofit = new Retrofit.Builder()
                //.baseUrl("http://keshavinfotechdemo1.com/keshav/KG2/WOW_Project/wow/tcapi/")
                .baseUrl("https://wow.wow2all.com/tcapi/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();
        return retrofit.create(UserService.class);
    }
}
