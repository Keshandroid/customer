package com.wow.alcom.activity;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.content.res.Configuration;
import android.os.Build;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.hbb20.CountryCodePicker;
import com.wow.alcom.R;
import com.wow.alcom.model.ContryCode;
import com.wow.alcom.model.LoginUser;
import com.wow.alcom.model.ResponseMessge;
import com.wow.alcom.model.User;
import com.wow.alcom.retrofit.APIClient;
import com.wow.alcom.retrofit.GetResult;
import com.wow.alcom.utils.CustPrograssbar;
import com.wow.alcom.utils.SessionManager;
import com.google.firebase.FirebaseApp;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Locale;
import java.util.Objects;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;

import static com.wow.alcom.utils.SessionManager.fbtoken;
import static com.wow.alcom.utils.SessionManager.login;
import static com.wow.alcom.utils.Utility.isvarification;

public class LoginActivity extends AppCompatActivity implements GetResult.MyListener {

    @BindView(R.id.txt_slogin)
    TextView txtSlogin;
    /*@BindView(R.id.clGoogleLogin)
    TextView clGoogleLogin;*/
    @BindView(R.id.txt_sregister)
    TextView txtSregister;
    @BindView(R.id.lvl_login)
    LinearLayout lvlLogin;
    @BindView(R.id.ed_email)
    EditText edEmail;
    @BindView(R.id.ed_passsword)
    EditText edPasssword;
    @BindView(R.id.txt_forgotpassword)
    TextView txtForgotpassword;
    @BindView(R.id.txt_login)
    TextView txtLogin;
    @BindView(R.id.lvl_register)
    LinearLayout lvlRegister;
    @BindView(R.id.ed_name)
    EditText edName;
    @BindView(R.id.ed_emailnew)
    EditText edEmailnew;
    @BindView(R.id.ed_mobile)
    EditText edMobile;
    @BindView(R.id.ed_passswordnew)
    EditText edPassswordnew;
    @BindView(R.id.ed_refercode)
    EditText edRefercode;
    @BindView(R.id.txt_register)
    TextView txtRegister;
    /*@BindView(R.id.at_code)
    AutoCompleteTextView atCode;*/
    CustPrograssbar custPrograssbar;
    SessionManager sessionManager;

    String lang = "";
    SharedPreferences languagepref, languageprefselect;
    TextView langg;
    EditText ed_mobile_login;
    GoogleSignInClient googleSignInClient;
    Integer RC_SIGN_IN = 1022;
    String newToken;
    CountryCodePicker txtCountryCode, txtCountryCodeLogin;

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;


    @SuppressLint("SetTextI18n")
    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ed_mobile_login = findViewById(R.id.ed_mobile_login);
        langg = findViewById(R.id.langg);
        txtCountryCode = findViewById(R.id.txtCountryCode);
        txtCountryCodeLogin = findViewById(R.id.txtCountryCodeLogin);
        ButterKnife.bind(this);
        FirebaseApp.initializeApp(this);

        //new permission
        //permission to access user for post
        String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};
        if (!hasPermissions(PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, MY_PERMISSIONS_REQUEST_LOCATION);
        }

        //old permission
        //requestPermissions(new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 1);

        custPrograssbar = new CustPrograssbar();
        languagepref = getSharedPreferences("language", MODE_PRIVATE);
        lang = languagepref.getString("languageToLoad", "");
        if (lang.equals("en")) {
            langg.setText(getString(R.string.lenar));
        } else {
            langg.setText(getString(R.string.len));
        }

        sessionManager = new SessionManager(LoginActivity.this);

        /*txtCountryCode.setOnClickListener(v -> {
            CharSequence cs[] = { "+91 India", "+974 Qatar" };

            new AlertDialog.Builder(this)
                    .setSingleChoiceItems(cs, 0, null)
                    .setPositiveButton("Ok", (dialog, whichButton) -> {
                        dialog.dismiss();
                        int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();

                        if(selectedPosition==0)txtCountryCode.setText("+91");
                        else if(selectedPosition==1)txtCountryCode.setText("+974");
                        // Do something useful withe the position of the selected radio button
                    })
                    .show();
        });
        txtCountryCodeLogin.setOnClickListener(v -> {
            CharSequence cs[] = { "+91 India", "+974 Qatar" };

            new AlertDialog.Builder(this)
                    .setSingleChoiceItems(cs, 0, null)
                    .setPositiveButton("Ok", (dialog, whichButton) -> {
                        dialog.dismiss();
                        int selectedPosition = ((AlertDialog)dialog).getListView().getCheckedItemPosition();

                        if(selectedPosition==0)txtCountryCodeLogin.setText("+91");
                        else if(selectedPosition==1)txtCountryCodeLogin.setText("+974");
                        // Do something useful withe the position of the selected radio button
                    })
                    .show();
        });*/

        /*atCode.setOnFocusChangeListener((view, b) -> {
            if (!b) {
                String str = atCode.getText().toString();
                ListAdapter listAdapter = atCode.getAdapter();
                for (int i = 0; i < listAdapter.getCount(); i++) {
                    String temp = listAdapter.getItem(i).toString();
                    if (str.compareTo(temp) == 0) {
                        return;
                    }
                }
                atCode.setText("");
            }
        });*/
        GoogleSignInOptions googleSignInOptions = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(getString(R.string.default_web_client_id))
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, googleSignInOptions);
        ConstraintLayout clGoogle = findViewById(R.id.clGoogleLogin);
        clGoogle.setOnClickListener(v -> {
            FirebaseMessaging.getInstance().getToken().addOnSuccessListener(token -> {
                if (!TextUtils.isEmpty(token)) {
                    newToken = token;
                    new SessionManager(this).setStringData(fbtoken, newToken);
                    /*Log.e("newToken", newToken);*/
                } else{
                    /*Log.w(TAG, "token should not be null...");*/
                }
            }).addOnFailureListener(e -> {
                //handle e
            }).addOnCanceledListener(() -> {
                //handle cancel
            }).addOnCompleteListener(task -> Log.d(TAG, "This is the token : " + task.getResult()));

            /*FirebaseInstanceId.getInstance().getInstanceId().addOnSuccessListener(LoginActivity.this, instanceIdResult -> {

            });*/

            configureGoogleClient();

            signInToGoogle();
        });
        getCodelist();
    }

    private void signInToGoogle() {
        Intent signInIntent = googleSignInClient.getSignInIntent();
        startActivityForResult(signInIntent, RC_SIGN_IN);
    }

    public void configureGoogleClient() {
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();
        googleSignInClient = GoogleSignIn.getClient(this, gso);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        try {
            if (requestCode == RC_SIGN_IN) {
                Task<GoogleSignInAccount> task = GoogleSignIn.getSignedInAccountFromIntent(data);
                try {
                    // Google Sign In was successful, authenticate with Firebase
                    GoogleSignInAccount account = task.getResult(ApiException.class);

                    Toast.makeText(LoginActivity.this, "Google Sign in Success", Toast.LENGTH_LONG).show();
                    assert account != null;
                    firebaseAuthWithGoogle(account);
                } catch (ApiException e) {
                    Log.d(TAG, "onActivityResult: test>>" + e.getMessage());
                    // Google Sign In failed, update UI appropriately
                    Toast.makeText(LoginActivity.this, "Unable to sign in. Try again later.", Toast.LENGTH_LONG).show();
                    //showToastMessage("Google Sign in Failed " + e);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private final String TAG = "LoginActivity";

    private void firebaseAuthWithGoogle(GoogleSignInAccount account) {




        custPrograssbar.prograssCreate(LoginActivity.this);
        JSONObject jsonObject = new JSONObject();
        try {

            String googleId = account.getId();
            String userName = account.getDisplayName();
            String email = account.getEmail();
            //String photoUrl = Objects.requireNonNull(account.getPhotoUrl()).toString();
            //String googleToken = account.getIdToken();

            jsonObject.put("google_id", googleId);
            jsonObject.put("name", userName);
            jsonObject.put("email", email);
            jsonObject.put("mobile", "");
            jsonObject.put("ccode", "");
            jsonObject.put("refercode", "");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Log.d(TAG, "firebaseAuthWithGoogle: >>>" + jsonObject);
        RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
        Call<JsonObject> call = APIClient.getInterface().getGoogleLogin(bodyRequest);
        GetResult getResult = new GetResult();
        getResult.setMyListener(this);
        getResult.callForLogin(call, "5");

        /*sessionManager.setBooleanData(login, true);
        startActivity(new Intent(LoginActivity.this, HomeActivity.class));
        finish();*/
        //callLogin(googleId, userName, email, photoUrl, googleToken);
    }

    private void getCodelist() {
        /*custPrograssbar.prograssCreate(LoginActivity.this);
        JSONObject jsonObject = new JSONObject();
        RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
        Call<JsonObject> call = APIClient.getInterface().getCodelist(bodyRequest);
        GetResult getResult = new GetResult();
        getResult.setMyListener(this);
        getResult.callForLogin(call, "2");*/

    }

    private void loginUser() {
        custPrograssbar.prograssCreate(LoginActivity.this);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("mobile", edEmail.getText().toString());
            jsonObject.put("password", edPasssword.getText().toString());
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
        Call<JsonObject> call = APIClient.getInterface().loginUser(bodyRequest);
        GetResult getResult = new GetResult();
        getResult.setMyListener(this);
        getResult.callForLogin(call, "1");
    }

    private void checkUser() {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("mobile", "+" + txtCountryCode.getSelectedCountryCode() + edMobile.getText().toString());
            /*jsonObject.put("ccode", txtCountryCode.getSelectedCountryCode());*/
            RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
            Call<JsonObject> call = APIClient.getInterface().getMobileCheck(bodyRequest);
            GetResult getResult = new GetResult();
            getResult.setMyListener(this);
            getResult.callForLogin(call, "3");
            custPrograssbar.prograssCreate(LoginActivity.this);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @OnClick({R.id.txt_slogin, R.id.txt_sregister, R.id.txt_register, R.id.txt_login, R.id.txt_forgotpassword, R.id.txt_skip_login})
    public void onClick(View view) {
        Log.e(TAG, "onClick() called with: view = [" + view + "]");
        switch (view.getId()) {
            case R.id.txt_skip_login:
                //sessionManager.setUserDetails("", loginUser.getUser());
                sessionManager.setBooleanData(login, true);
                startActivity(new Intent(LoginActivity.this, HomeActivity.class));
                finish();
                break;

            case R.id.txt_slogin:
                txtSlogin.setBackground(getResources().getDrawable(R.drawable.tab1));
                txtSlogin.setTextColor(getResources().getColor(R.color.white));
                txtSregister.setBackground(getResources().getDrawable(R.drawable.tab2));
                txtSregister.setTextColor(getResources().getColor(R.color.black));
                lvlLogin.setVisibility(View.VISIBLE);
                lvlRegister.setVisibility(View.GONE);

                break;

            case R.id.txt_sregister:
                txtSlogin.setBackground(getResources().getDrawable(R.drawable.tab2));
                txtSlogin.setTextColor(getResources().getColor(R.color.black));
                txtSregister.setBackground(getResources().getDrawable(R.drawable.tab1));
                txtSregister.setTextColor(getResources().getColor(R.color.white));
                lvlLogin.setVisibility(View.GONE);
                lvlRegister.setVisibility(View.VISIBLE);
                break;

            case R.id.txt_register:
                if (validationCreate()) {
                    checkUser();
                }
                break;
            case R.id.txt_login:
                loginWithPhoneNumber();

                /*if (validation()) {
                    loginUser();
                }*/
                break;
            case R.id.txt_forgotpassword:
                startActivity(new Intent(LoginActivity.this, ForgotActivity.class));
                break;
            default:
                break;
        }
    }

    private void loginWithPhoneNumber() {

        custPrograssbar.prograssCreate(LoginActivity.this);
        JSONObject jsonObject = new JSONObject();
        try {
            String str = "+" + txtCountryCodeLogin.getSelectedCountryCode() + ed_mobile_login.getText().toString();
            jsonObject.put("mobile", str);
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
        Call<JsonObject> call = APIClient.getInterface().userLoginPhNo(bodyRequest);
        GetResult getResult = new GetResult();
        getResult.setMyListener(this);
        getResult.callForLogin(call, "6");

    }

    @Override
    public void callback(JsonObject result, String callNo) {
        try {
            custPrograssbar.closePrograssBar();
            if (callNo.equalsIgnoreCase("6")) {
                try {
                    JSONObject obj = new JSONObject(result.toString());
                    if (obj.getString("Result").equalsIgnoreCase("false")) {
                        Toast.makeText(this, obj.getString("ResponseMsg"), Toast.LENGTH_LONG).show();
                    } else {
                        startActivity(new Intent(this, VerifyPhoneActivity.class).putExtra("number", "+" + txtCountryCodeLogin.getSelectedCountryCode() + ed_mobile_login.getText().toString()));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Log.d(TAG, "callback: testAb>>" + result.toString());
            } else if (callNo.equalsIgnoreCase("5")) {
                Gson gson = new Gson();
                LoginUser loginUser = gson.fromJson(result.toString(), LoginUser.class);
                Toast.makeText(LoginActivity.this, loginUser.getResponseMsg(), Toast.LENGTH_LONG).show();
                if (loginUser.getResult().equalsIgnoreCase("true")) {
                    sessionManager.setUserDetails("", loginUser.getUser());
                    sessionManager.setBooleanData(login, true);
                    OneSignal.sendTag("userid", loginUser.getUser().getId());
                    startActivity(new Intent(LoginActivity.this, HomeActivity.class).putExtra("from","login").setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                }
            } else if (callNo.equalsIgnoreCase("1")) {
                Gson gson = new Gson();
                LoginUser loginUser = gson.fromJson(result.toString(), LoginUser.class);
                Toast.makeText(LoginActivity.this, loginUser.getResponseMsg(), Toast.LENGTH_LONG).show();
                if (loginUser.getResult().equalsIgnoreCase("true")) {
                    sessionManager.setUserDetails("", loginUser.getUser());
                    sessionManager.setBooleanData(login, true);
                    OneSignal.sendTag("userid", loginUser.getUser().getId());
                    startActivity(new Intent(LoginActivity.this, HomeActivity.class).setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK));
                }
            } else if (callNo.equalsIgnoreCase("2")) {
                Gson gson = new Gson();
                ContryCode contryCode = gson.fromJson(result.toString(), ContryCode.class);
                ArrayList<String> countries = new ArrayList<>();
                if (contryCode.getResult().equalsIgnoreCase("true")) {
                    countries = new ArrayList<>();
                    for (int i = 0; i < contryCode.getCountryCode().size(); i++) {
                        countries.add(contryCode.getCountryCode().get(i).getCcode());
                    }
                }
                ArrayAdapter<String> adapter = new ArrayAdapter<>
                        (this, android.R.layout.simple_list_item_1, countries);
                //atCode.setAdapter(adapter);
                //atCode.setThreshold(1);
            } else if (callNo.equalsIgnoreCase("3")) {
                Gson gson = new Gson();
                ResponseMessge response = gson.fromJson(result.toString(), ResponseMessge.class);
                if (response.getResult().equals("true")) {
                    isvarification = 1;
                    User user = new User();
                    user.setName(edName.getText().toString());
                    user.setEmail(edEmailnew.getText().toString());
                    user.setCcode(txtCountryCode.getSelectedCountryCode());
                    user.setMobile(edMobile.getText().toString());
                    user.setPassword(edPassswordnew.getText().toString());
                    user.setRefercode(edRefercode.getText().toString());
                    sessionManager.setUserDetails("", user);
                    /*atCode.getText().toString()*/
                    startActivity(new Intent(this, VerifyPhoneActivity.class).putExtra("code", "+" + txtCountryCode.getSelectedCountryCode() + edMobile.getText().toString()));
                } else {
                    Toast.makeText(LoginActivity.this, "" + response.getResponseMsg(), Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception e) {
            Log.e("Errror", "==>" + e.toString());

        }
    }

    public boolean validation() {
        if (TextUtils.isEmpty(edEmail.getText().toString())) {
            edEmail.setError("Enter Email");
            return false;
        }
        if (TextUtils.isEmpty(edPasssword.getText().toString())) {
            edPasssword.setError("Enter Password");
            return false;
        }
        return true;
    }

    public boolean validationCreate() {
        if (TextUtils.isEmpty(edName.getText().toString())) {
            edName.setError("Enter Name");
            return false;
        }
        if (TextUtils.isEmpty(edEmailnew.getText().toString())) {
            edEmailnew.setError("Enter Email");
            return false;
        }
        if (TextUtils.isEmpty(edMobile.getText().toString())) {
            edMobile.setError("Enter Mobile");
            return false;
        }
        if (TextUtils.isEmpty(edPassswordnew.getText().toString())) {
            edPassswordnew.setError("Enter Password");
            return false;
        }
        /*if (txtCountryCode.getSelectedCountryCode().equals("+91") || txtCountryCode.getSelectedCountryCode().equals("+974")) {
            if (TextUtils.isEmpty(edPassswordnew.getText().toString())) {
                edPassswordnew.setError("Enter Password");
                return false;
            }
        }else{
            edMobile.setError("Only Qatar and Indian Mobile Numbers are allowed");
            return false;
        }*/

        return true;
    }


    public void lEng(View view) {

        if (lang.equals("en")) {
            langg.setText(getString(R.string.lenar));
            updateEnglish("ar");
            languageprefselect = getSharedPreferences("lang", MODE_PRIVATE);
            SharedPreferences.Editor editor = languageprefselect.edit();
            editor.putString("ar", "ARB");
            editor.apply();
            editor.commit();


        } else {
            langg.setText(getString(R.string.len));
            updateEnglish("en");
            languageprefselect = getSharedPreferences("lang", MODE_PRIVATE);
            SharedPreferences.Editor editor = languageprefselect.edit();
            editor.putString("ar", "ENG");
            editor.apply();
            editor.commit();


        }


    }


    private void updateLanguage(String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        SharedPreferences.Editor editor = languagepref.edit();
        editor.putString("languageToLoad", language);
        editor.apply();
        finish();


    }


    private void updateEnglish(String language) {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        languagepref = getSharedPreferences("language", MODE_PRIVATE);
        SharedPreferences.Editor editor = languagepref.edit();
        editor.putString("languageToLoad", language);
        editor.apply();
        finish();

        overridePendingTransition(0, 0);
        startActivity(getIntent());
        overridePendingTransition(0, 0);


    }


    private boolean hasPermissions(String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null) {
            for (String permission : permissions)
                if (ActivityCompat.checkSelfPermission(LoginActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case MY_PERMISSIONS_REQUEST_LOCATION:
                    //getCurrentLocation();
                    break;
                default:
                    break;
            }
        } else {
            Toast.makeText(LoginActivity.this, "Permission Denied : services will not be visible ", Toast.LENGTH_SHORT).show();
        }
    }








}