package com.wow.alcom.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.activity.result.ActivityResultLauncher;
import androidx.activity.result.contract.ActivityResultContracts;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.github.dewinjm.monthyearpicker.MonthYearPickerDialog;
import com.github.dewinjm.monthyearpicker.MonthYearPickerDialogFragment;
import com.stripe.android.PaymentConfiguration;
import com.stripe.android.Stripe;
import com.stripe.android.view.CardInputWidget;
import com.wow.alcom.R;
import com.wow.alcom.model.PaymentItem;
import com.wow.alcom.ooredooCard.PaymentController;
import com.wow.alcom.ooredooCard.ReceiptWebViewActivity;
import com.wow.alcom.ooredooCard.listener.IPaymentControllerListener;
import com.wow.alcom.ooredooCard.model.PaymentResp;
import com.wow.alcom.ooredooCard.model.Response;
import com.wow.alcom.ooredooCard.view.IPaymentView;
import com.wow.alcom.utils.CustPrograssbar;
import com.wow.alcom.utils.SessionManager;

import java.text.DecimalFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class OoredooCardPaymentActivity extends BaseActivity implements IPaymentView {

    private static final String TAG="OoredooCardPaymentActiv";

    @BindView(R.id.txt_total)
    TextView txtTotal;
    private String paymentIntentClientSecret;
    private Stripe stripe;
    CardInputWidget cardInputWidget;
    PaymentItem paymentItem;
    double amountDouble = 0.0;
    SessionManager sessionManager;
    CustPrograssbar custPrograssbar;
    TextView txtCardDetails;
    private EditText edtPhone, edtCard, edtCVV, txtExpiry,edtEmail,edtName;
    private LinearLayout linCard;
    private RadioGroup radioGroup;
    private IPaymentControllerListener controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ooredoo_card);
        ButterKnife.bind(this);

        edtEmail = findViewById(R.id.edtEmail);
        edtName = findViewById(R.id.edtName);
        edtCard = findViewById(R.id.edtCard);
        edtCVV = findViewById(R.id.edtCVV);
        txtExpiry = findViewById(R.id.txtExpiry);
        edtPhone = findViewById(R.id.edtPhone);
        linCard = findViewById(R.id.linCard);
        txtCardDetails = findViewById(R.id.txtCardDetails);
        radioGroup = findViewById(R.id.rg);
        controller = new PaymentController(this, this);

        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int rbId = radioGroup.getCheckedRadioButtonId();
                String cardType = "";
                if (rbId == R.id.rbCredit || rbId == R.id.rbDebit) {
                    linCard.setVisibility(View.VISIBLE);
                    txtCardDetails.setVisibility(View.VISIBLE);
                    edtPhone.setVisibility(View.GONE);
                } else if (rbId == R.id.rbOmm) {
                    linCard.setVisibility(View.GONE);
                    txtCardDetails.setVisibility(View.GONE);
                    edtPhone.setVisibility(View.VISIBLE);
                }
            }
        });
        findViewById(R.id.btnPay).setOnClickListener(v -> {
            String card = edtCard.getText().toString();
            String cvv = edtCVV.getText().toString();
            String expiryDate = txtExpiry.getText().toString();
            String name = edtName.getText().toString();
            String email = edtEmail.getText().toString();
            String amount =  new DecimalFormat("##.##").format(amountDouble);
            String phone = edtPhone.getText().toString();
            if (validate(email, amount, card, cvv, expiryDate, name))
                callAPI(email, amount, card, cvv, expiryDate, name, phone);
        });

        findViewById(R.id.txtExpiry).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Calendar calendar = Calendar.getInstance();
                int yearSelected;
                int monthSelected;

                yearSelected = calendar.get(Calendar.YEAR);
                monthSelected = calendar.get(Calendar.MONTH);

                long minDate = Calendar.getInstance().getTimeInMillis();
                MonthYearPickerDialogFragment dialogFragment = MonthYearPickerDialogFragment
                        .getInstance(monthSelected, yearSelected, minDate, MonthYearPickerDialogFragment.NULL_INT);

                dialogFragment.show(getSupportFragmentManager(), null);
                dialogFragment.setOnDateSetListener(new MonthYearPickerDialog.OnDateSetListener() {
                    @Override
                    public void onDateSet(int year, int monthOfYear) {
                        Log.d(TAG, "Year " + year + " Month: " + (monthOfYear + 1));
                        txtExpiry.setText(String.format("%02d", (monthOfYear + 1)) + "/" + year);
                    }
                });
            }
        });

        custPrograssbar = new CustPrograssbar();
        sessionManager = new SessionManager(OoredooCardPaymentActivity.this);
        paymentItem = (PaymentItem) getIntent().getSerializableExtra("detail");
        amountDouble = getIntent().getDoubleExtra("amount", 0.0);
        txtTotal.setText("Total Amount : " + sessionManager.getStringData(SessionManager.currency) + new DecimalFormat("##.##").format(amountDouble));
        long total = Math.round(amountDouble * 100);
        Log.e("toto", "-->" + total);
        List<String> elephantList = Arrays.asList(paymentItem.getmAttributes().split(","));
        PaymentConfiguration.init(
                getApplicationContext(),
                elephantList.get(0)
        );


        /*new Thread() {
            @Override
            public void run() {
                try {
                    com.stripe.Stripe.apiKey = elephantList.get(1);
                    PaymentIntentCreateParams params =
                            PaymentIntentCreateParams.builder()
                                    .setAmount(total)
                                    .setCurrency("INR")
                                    .build();
                    PaymentIntent intent = PaymentIntent.create(params);
                    paymentIntentClientSecret = intent.getClientSecret();

                    // Hook up the pay button to the card widget and stripe instance
                    cardInputWidget = findViewById(R.id.cardInputWidget);
                    Button payButton = findViewById(R.id.payButton);
                    payButton.setOnClickListener(view -> {
                        custPrograssbar.prograssCreate(OoredooCardPaymentActivity.this);
                        PaymentMethodCreateParams params1 = cardInputWidget.getPaymentMethodCreateParams();
                        if (params1 != null) {
                            ConfirmPaymentIntentParams confirmParams = ConfirmPaymentIntentParams
                                    .createWithPaymentMethodCreateParams(params1, paymentIntentClientSecret);
                            final Context context = OoredooCardPaymentActivity.this.getApplicationContext();
                            stripe = new Stripe(
                                    context,
                                    PaymentConfiguration.getInstance(context).getPublishableKey()
                            );
                            stripe.confirmPayment(OoredooCardPaymentActivity.this, confirmParams);
                        }
                    });
                } catch (StripeException e) {
                    e.printStackTrace();
                }
            }
        }.start();*/


    }

    private boolean validate(String email, String amount, String card, String cvv, String expiryDate, String name) {
        int rbId = radioGroup.getCheckedRadioButtonId();
        if (rbId == -1) {
            Toast.makeText(this,"Please select card type",Toast.LENGTH_LONG).show();
            return false;
        } else if (email.isEmpty()) {
            Toast.makeText(this,"Please enter email",Toast.LENGTH_LONG).show();
            return false;
        } else if (!isValidEmail(email)) {
            Toast.makeText(this,"Enter valid email",Toast.LENGTH_LONG).show();
            return false;
        } else if (amount.isEmpty()) {
            Toast.makeText(this,"Enter amount",Toast.LENGTH_LONG).show();
            return false;
        } else if (card.isEmpty()) {
            Toast.makeText(this,"Enter card number",Toast.LENGTH_LONG).show();
            return false;
        } else if (card.length() < 16) {
            Toast.makeText(this,"Enter valid card number",Toast.LENGTH_LONG).show();
            return false;
        } else if (cvv.isEmpty()) {
            Toast.makeText(this,"Enter CVV",Toast.LENGTH_LONG).show();
            return false;
        } else if (cvv.length() < 3) {
            Toast.makeText(this,"Enter valid CVV",Toast.LENGTH_LONG).show();
            return false;
        } else if (expiryDate.isEmpty()) {
            Toast.makeText(this,"Select expiry date",Toast.LENGTH_LONG).show();
            return false;
        } else if (name.isEmpty()) {
            Toast.makeText(this,"Enter name",Toast.LENGTH_LONG).show();
            return false;
        } else
            return true;
    }
    public static boolean isValidEmail(String email) {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(email).matches();
    }
    String amount;
    private void callAPI(String email, String amount, String card, String cvv, String expiryDate, String name, String phone) {
this.amount=amount;
        String expMonth = "";
        String expYr = "";
        if (!expiryDate.isEmpty()) {
            expMonth = expiryDate.split("/")[0];
            expYr = expiryDate.split("/")[1];
        }

        //This is unique id for each transaction generated by our server
        String trackId = System.currentTimeMillis()+"";

        String requestBodyText = "<request>\n" +
                "<terminalid>WT1</terminalid>\n" +
                "<password>541585</password>\n" +
                "<action>1</action>\n" +
                "<card>" + card + "</card>\n" +
                "<cvv2>" + cvv + "</cvv2>\n" +
                "<expYear>" + expYr + "</expYear>\n" +
                "<expMonth>" + expMonth + "</expMonth>\n" +
                "<member>" + name + "Test User</member>\n" +
                "<currencyCode>QAR</currencyCode>\n" +
                "<email>" + email + "</email>\n" +
                "<amount>" + amount + "</amount>\n" +
                "<trackid>" + trackId + "</trackid>\n" +
                "<udf1></udf1>\n" +
                "<udf2></udf2>\n" +
                "<udf3></udf3>\n" +
                "<udf4></udf4>\n" +
                "</request>";

        String mobilePaymentReq = "<request>\n" +
                "<terminalid>WT1</terminalid>\n" +
                "<password>541585</password>\n" +
                "<action>1</action>\n" +
                "<member>Test User</member>\n" +
                "<currencyCode>QAR</currencyCode>\n" +
                "<CountryCode>QR</CountryCode>\n" +
                "<email>" + email + "</email>\n" +
                "<amount>" + amount + "</amount>\n" +
                "<trackid>" + System.currentTimeMillis() + "</trackid>\n" +
                "<phonenumber>" + phone + "</phonenumber>\n" +
                "<udf1></udf1>\n" +
                "<udf2></udf2>\n" +
                "<udf3></udf3>\n" +
                "<udf4></udf4>\n" +
                "</request>";

        int rbId = radioGroup.getCheckedRadioButtonId();
        String cardType = "";
        if (rbId == R.id.rbCredit || rbId == R.id.rbDebit) {
            controller.callPaymentApi(requestBodyText);
        } else if (rbId == R.id.rbOmm) {
            controller.callMobilePaymentApi(mobilePaymentReq);
        }

    }

    @Override
    public void onSuccess(PaymentResp paymentResp) {
        try {
            Response data = paymentResp.getResponse();
            if (data.getPayId() != null && !data.getPayId().isEmpty() && data.getTargetUrl() != null && !data.getTargetUrl().isEmpty()) {
                String payId = data.getPayId();
                String targetUrl = data.getTargetUrl();
                String trackId = data.getTrackid();
                String tranId = data.getTranid();
                String receiptUrl = targetUrl + payId;
                Intent i = new Intent(OoredooCardPaymentActivity.this, ReceiptWebViewActivity.class);
                i.putExtra("url", receiptUrl);
//                startActivity(i);
                Intent intent = new Intent(this, ReceiptWebViewActivity.class);
                intent.putExtra("url", receiptUrl);
                intent.putExtra("trackId", trackId);
                intent.putExtra("tranId", tranId);
                intent.putExtra("amount", amount);
                someActivityResultLauncher.launch(intent);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    ActivityResultLauncher<Intent> someActivityResultLauncher = registerForActivityResult(
            new ActivityResultContracts.StartActivityForResult(),
            result -> {
                if (result.getResultCode() == Activity.RESULT_OK) {
                    // There are no request codes
                    Intent data = result.getData();
                    String responseCode = data.getStringExtra("responsecode");
                    String transactionResult = data.getStringExtra("result");
                    String trackId = data.getStringExtra("trackid"); //This will be used and saved in our db
                    String tranId = data.getStringExtra("tranid"); //This will be used and saved in our db - same we have used for payment
                    String receipt_url = data.getStringExtra("receipt_url"); //This will be used and saved in our db - same we have used for payment
                    System.out.println(TAG + "ResponseCode:" + responseCode);
                    if (transactionResult.equals("SUCCESS") && responseCode.equals("000")) {
                        Toast.makeText(getApplicationContext(), "Payment Success", Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(getApplicationContext(), "Payment Failed", Toast.LENGTH_SHORT).show();
                    }
                    Intent i = new Intent(OoredooCardPaymentActivity.this, TimeSlotActivity.class);
                    i.putExtra("receipt_url",receipt_url);
                    i.putExtra("trackId", trackId);
                    i.putExtra("tranId", tranId);
                    i.putExtra("amount", amount);
                    setResult(RESULT_OK, i);
                    finish();
                }
            });

    @Override
    public void onFailed(String message) {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void dismissProgress() {

    }
}