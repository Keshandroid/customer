package com.wow.alcom.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.GsonBuilder;
import com.wow.alcom.R;
import com.wow.alcom.adepter.AddonsAdapter;
import com.wow.alcom.model.AddOnDataItem;
import com.wow.alcom.model.Addon;
import com.wow.alcom.model.ServiceListItem;
import com.wow.alcom.model.User;
import com.wow.alcom.retrofit.APIClient;
import com.wow.alcom.retrofit.GetResult;
import com.wow.alcom.utils.CustPrograssbar;
import com.wow.alcom.utils.MyDatabase;
import com.wow.alcom.utils.SessionManager;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;

public class AddOnsActivity extends BaseActivity implements AddonsAdapter.RecyclerTouchListener, GetResult.MyListener {

    @BindView(R.id.recycle_addone)
    RecyclerView recycleAddone;
    @BindView(R.id.lvl_next)
    LinearLayout lvlNext;

    @BindView(R.id.txt_qty)
    TextView txtQty;
    @BindView(R.id.txt_totle)
    TextView txtTotle;


    String cid;
    String name;
    CustPrograssbar custPrograssbar;
    SessionManager sessionManager;
    User user;
    AddonsAdapter adapter;
    SharedPreferences language;
    MyDatabase myDatabase;
    int totle = 0;

    public void cardData(List<ServiceListItem> list) {
        if (list.isEmpty()) {

        } else {
            int qty = 0;
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getServiceCatId().equalsIgnoreCase(cid)) {
                    double res3 = (list.get(i).getServicePrice() / 100.0f) * list.get(i).getServiceDiscount();
                    double pp = list.get(i).getServicePrice() - res3;
                    pp = pp * Integer.parseInt(list.get(i).getServiceQty());
                    totle = (int) (totle + pp);
                    qty = qty + 1;

                }
            }
            txtQty.setText("" + qty);
            txtTotle.setText(sessionManager.getStringData(SessionManager.currency) + totle);

        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_ons);
        ButterKnife.bind(this);
        name = getIntent().getStringExtra("name");
        myDatabase = new MyDatabase(AddOnsActivity.this);
        getSupportActionBar().setTitle(name);
        custPrograssbar = new CustPrograssbar();
        sessionManager = new SessionManager(this);
        user = sessionManager.getUserDetails("");
        cid = getIntent().getStringExtra("cid");
        language = getSharedPreferences("lang", MODE_PRIVATE);
        getAdon();
        cardData(myDatabase.getCData(cid));
        recycleAddone.setLayoutManager(new GridLayoutManager(this, 2));
        recycleAddone.setItemAnimator(new DefaultItemAnimator());

    }


    private void getAdon() {
        custPrograssbar.prograssCreate(this);
        JSONObject jsonObject = new JSONObject();
        try {
            if (user.getId() == null || user.getId().equals("")) {
                jsonObject.put("uid", "");
            } else {
                jsonObject.put("uid", user.getId());
            }
            jsonObject.put("cid", cid);
            jsonObject.put("lang", language.getString("ar", ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
        Call<JsonObject> call = APIClient.getInterface().listaddon(bodyRequest);
        GetResult getResult = new GetResult();
        getResult.setMyListener(this);
        getResult.callForLogin(call, "1");

    }

    @Override
    public void onClickAddonsItem(String title, int position) {


    }

    @Override
    public void callback(JsonObject result, String callNo) {
        try {
            custPrograssbar.closePrograssBar();
            if (callNo.equalsIgnoreCase("1")) {

                Log.e("AdOnsResult", "-->" + result.toString());


                Gson gson = new Gson();
                Addon addon = gson.fromJson(result.toString(), Addon.class);

                adapter = new AddonsAdapter(AddOnsActivity.this, addon.getAddondata(), this);
                recycleAddone.setAdapter(adapter);
            }
        } catch (Exception e) {
            Log.e("error", "-->" + e.toString());
        }
    }


    @OnClick({R.id.lvl_next})
    public void onClick(View view) {
        if (view.getId() == R.id.lvl_next) {

            if (totle < 100) {
                Toast.makeText(AddOnsActivity.this, "Add items more than 99 QAR to checkout", Toast.LENGTH_LONG).show();
            } else {

                ArrayList<AddOnDataItem> list = new ArrayList<>();
                ArrayList<AddOnDataItem> listq = new ArrayList<>();
                try {
                    list = adapter.getSelectItem();
                    for (int i = 0; i < list.size(); i++) {
                        if (list.get(i).isSelected()) {
                            listq.add(list.get(i));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }

                if (!listq.isEmpty()) {
                    startActivity(new Intent(AddOnsActivity.this, CartActivity.class)
                            .putExtra("name", name)
                            .putExtra("cid", cid)
                            .putParcelableArrayListExtra("slist", listq));
                } else {
                    startActivity(new Intent(AddOnsActivity.this, CartActivity.class)
                            .putExtra("name", name)
                            .putExtra("cid", cid)
                            .putParcelableArrayListExtra("slist", listq));
                }
            }
        }
    }
}