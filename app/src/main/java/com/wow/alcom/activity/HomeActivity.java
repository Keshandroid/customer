package com.wow.alcom.activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.LinearLayoutCompat;
import androidx.core.app.ActivityCompat;
import androidx.fragment.app.Fragment;

import com.wow.alcom.R;
import com.wow.alcom.fragment.BottomSheetChatDialog;
import com.wow.alcom.fragment.HomeFragment;
import com.wow.alcom.model.User;
import com.wow.alcom.utils.Identity;
import com.wow.alcom.utils.SessionManager;
import com.wow.alcom.utils.Utility;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

public class HomeActivity extends BaseActivity implements BottomNavigationView.OnNavigationItemSelectedListener {

    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    //update app dialog
    String currentVersion,latestVersion;
    private Dialog popup;


    @RequiresApi(api = Build.VERSION_CODES.M)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().getDecorView().setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_STABLE | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN);
        setContentView(R.layout.activity_home);


        //new permission
        //permission to access user for post
        String[] PERMISSIONS = {Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION};
        if (!hasPermissions(PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS, MY_PERMISSIONS_REQUEST_LOCATION);
        }

        //old premission
        //requestPermissions(new String[]{Manifest.permission.CALL_PHONE, Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION}, 1);



        final LocationManager manager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
        if (!manager.isProviderEnabled(LocationManager.GPS_PROVIDER) && Utility.hasGPSDevice(this)) {
            Toast.makeText(this, "Gps not enabled", Toast.LENGTH_SHORT).show();
            Utility.enableLoc(this);
        }
        loadFragment(new HomeFragment());
        BottomNavigationView navigation = findViewById(R.id.navigation);
        navigation.setOnNavigationItemSelectedListener(this);

        checkFromLogin();

        getCurrentVersion();

    }

    private void getCurrentVersion() {
        PackageManager pm = this.getPackageManager();
        PackageInfo pInfo = null;
        try {
            pInfo =  pm.getPackageInfo(this.getPackageName(),0);
        } catch (PackageManager.NameNotFoundException e1) {
            e1.printStackTrace();
        }
        currentVersion = pInfo.versionName;

        new GetLatestVersion().execute();

    }

    private class GetLatestVersion extends AsyncTask<String, String, JSONObject> {

        private ProgressDialog progressDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected JSONObject doInBackground(String... params) {
            try {
//It retrieves the latest version by scraping the content of current version from play store at runtime
                Document doc = Jsoup.connect("https://play.google.com/store/apps/details?id=com.wow.alcom&hl=en_IN&gl=US").get();
                latestVersion = doc.getElementsByClass("htlgb").get(6).text();
                //printLog(TAG,"=== latest version===" + latestVersion);

            }catch (Exception e){
                e.printStackTrace();

            }

            return new JSONObject();
        }

        @Override
        protected void onPostExecute(JSONObject jsonObject) {

            if(latestVersion!=null) {

                if (!currentVersion.equalsIgnoreCase(latestVersion)){
                    if(!isFinishing()){
                        //This would help to prevent Error : BinderProxy@45d459c0 is not valid; is your activity running? error

                        Log.d("VERSION_CHECK", "UPDATE THE APP");

                        if(!Identity.getUpdateDialog(HomeActivity.this).equals("update_later")){
                            showUpdateDialog();
                        }
                    }
                } else {
                    Log.d("VERSION_CHECK", "CONTINUE OPEN APP");
                }
            }
            else
                //background.start();
                super.onPostExecute(jsonObject);
        }
    }

    public void showUpdateDialog() {

        try {
            if (!isFinishing()) {
                popup = new Dialog(this, R.style.DialogCustom);
                popup.setContentView(R.layout.dialog_update_app);
                popup.setCanceledOnTouchOutside(false);
                popup.setCancelable(false);
                popup.show();

                TextView txtNoThanks = popup.findViewById(R.id.txtNoThanks);
                Button btnUpdate = popup.findViewById(R.id.btnUpdate);

                txtNoThanks.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Identity.setUpdateDialog(HomeActivity.this, "update_later");
                        popup.dismiss();
                    }
                });

                btnUpdate.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        popup.dismiss();

                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.wow.alcom")));
                        } catch (Exception e) {
                            Toast.makeText(HomeActivity.this, "Unable to Connect Try Again...",Toast.LENGTH_SHORT).show();
                            e.printStackTrace();
                        }
                    }
                });
            }
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(bottomSheet!=null)bottomSheet.onActivityResult(requestCode,resultCode,data);
    }
    BottomSheetChatDialog bottomSheet;
    private void checkFromLogin() {


        try {
            if (getIntent().getStringExtra("from").equalsIgnoreCase("login")) {
                SessionManager sessionManager = new SessionManager(this);
                User user = sessionManager.getUserDetails("");
                if(user!=null && user.getId()!=null && user.getName()!=null){
                    bottomSheet = new BottomSheetChatDialog(HomeActivity.this);
                    bottomSheet.show(getSupportFragmentManager(),
                            "ModalBottomSheet");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean loadFragment(Fragment fragment) {
        //switching fragment
        if (fragment != null) {
            getSupportFragmentManager()
                    .beginTransaction()
                    .replace(R.id.fragment_container, fragment)
                    .commit();
            return true;
        }
        return false;
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        User ab = new SessionManager(HomeActivity.this).getUserDetails("");
        Fragment fragment = null;
        switch (item.getItemId()) {
            case R.id.page_1:
                fragment = new HomeFragment();
                break;
            case R.id.page_2:
                if (ab == null || ab.getId() == null || ab.getId().equals("0")) {
                    Utility.showDialogForLogin(this);
                } else {
                    startActivity(new Intent(this, BookingActivity.class));
                }
                break;
            case R.id.page_3:
                if (ab == null || ab.getId() == null || ab.getId().equals("0")) {
                    Utility.showDialogForLogin(this);
                } else {
                    startActivity(new Intent(HomeActivity.this, ReferlActivity.class));
                }
                break;
            case R.id.page_4:
                if (ab == null || ab.getId() == null || ab.getId().equals("0")) {
                    Utility.showDialogForLogin(this);
                } else {
                    startActivity(new Intent(HomeActivity.this, NotificationActivity.class));
                }
                break;
            case R.id.page_5:
                startActivity(new Intent(this, SettingActivity.class));
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + item.getItemId());
        }

        return loadFragment(fragment);
    }

    private final String TAG = "HomeActivity";


    private boolean hasPermissions(String... permissions) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && permissions != null) {
            for (String permission : permissions)
                if (ActivityCompat.checkSelfPermission(HomeActivity.this, permission) != PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            switch (requestCode) {
                case MY_PERMISSIONS_REQUEST_LOCATION:
                    //getCurrentLocation();
                    break;
                default:
                    break;
            }
        } else {
            Toast.makeText(HomeActivity.this, "Permission Denied : services will not be visible ", Toast.LENGTH_SHORT).show();
        }
    }

}