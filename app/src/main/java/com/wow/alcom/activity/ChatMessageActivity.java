package com.wow.alcom.activity;

import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.theartofdev.edmodo.cropper.CropImage;
import com.wow.alcom.R;
import com.wow.alcom.adepter.ChatMessagesAdapter;
import com.wow.alcom.chatModule.AudioRecorder;
import com.wow.alcom.chatModule.ChatModule;
import com.wow.alcom.chatModule.FilePicker;
import com.wow.alcom.chatModule.objects.ChatMsgObject;
import com.wow.alcom.model.User;
import com.wow.alcom.utils.SessionManager;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.Objects;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;
import kotlin.Unit;

public class ChatMessageActivity extends BaseActivity implements ChatModule.ChatMessageListResponse, ChatMessagesAdapter.IOnclickListner {

    FilePicker filePicker;
    ChatModule chatModule;
    AudioRecorder audioRecord;
    ChatMessagesAdapter adapterMessages;

    LinearLayout llMain;
    ImageView imgEmojiKB;
    RecyclerView rcvChatMessages;
    ImageView imgSend, imgPick, imgAudioRecord;
    hani.momanii.supernova_emoji_library.Helper.EmojiconEditText etxtMessage;
    String[] title = {
            "Video",
            "Image",
            "Cancel"
    };

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @SuppressLint("ClickableViewAccessibility")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_message);
        Objects.requireNonNull(getSupportActionBar()).setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(getResources().getString(R.string.menu7));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        llMain = findViewById(R.id.llMain);
        imgEmojiKB = findViewById(R.id.imgEmojiKB);
        rcvChatMessages = findViewById(R.id.rcvChatMessages);
        imgAudioRecord = findViewById(R.id.imgAudioRecord);
        etxtMessage = findViewById(R.id.etxtMessage);
        imgPick = findViewById(R.id.imgPick);
        imgSend = findViewById(R.id.imgSend);
        audioRecord = new AudioRecorder(this);

        EmojIconActions emojIcon = new EmojIconActions(this, llMain, etxtMessage, imgEmojiKB, "#2c003e", "#CFCFCF", "#E1DFDF");
        emojIcon.ShowEmojIcon();
        etxtMessage.setUseSystemDefault(true);

        imgSend.setOnClickListener(v -> {
            if (etxtMessage.getText().toString().trim().equalsIgnoreCase("")) {
                Toast.makeText(ChatMessageActivity.this, "Please enter message to continue.", Toast.LENGTH_LONG).show();
            } else {
                chatModule.sendStringMessage(etxtMessage.getText().toString().trim());
                etxtMessage.setText("");
            }
        });
        imgAudioRecord.setOnTouchListener((v, event) -> {

            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                audioRecord.startRecording();
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                audioRecord.stopRecording();
                audioUploadDialog(audioRecord.getAudioPath());
            }

            return true;
        });
        imgPick.setOnClickListener(v -> {
            String[] options = {"Video", "Image", "Cancel"};
            AlertDialog.Builder builder = new AlertDialog.Builder(ChatMessageActivity.this);
            builder.setItems(options, (dialog, which) -> {
                if (title[which].equalsIgnoreCase("Video")) {
                    filePicker = new FilePicker(ChatMessageActivity.this, 2);
                } else if (title[which].equalsIgnoreCase("Image")) {
                    filePicker = new FilePicker(ChatMessageActivity.this, 1);
                } else if (title[which].equalsIgnoreCase("Cancel")) {
                    dialog.dismiss();
                }
            });
            builder.show();
        });

        setupChatModule();
    }

    private static final String TAG = "ChatMessageActivity";
    Menu menu;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        getMenuInflater().inflate(R.menu.chat_menu, menu);
        setCreateMenu();
        return true;
    }

    private void setOptionMenu(Integer i) {
        if (i == 1) {
            String blockstatus = chatModule.blockUserStatus();
            if (blockstatus.equalsIgnoreCase("0")) {
                chatModule.blockUser(chatModule.getChatNode());
                setupChatModule();
                menu.findItem(R.id.blockUser).setTitle("Unblock User");
            } else if (blockstatus.equalsIgnoreCase("1")) {
                chatModule.unblockUser(chatModule.getChatNode());
                setupChatModule();
                menu.findItem(R.id.blockUser).setTitle("Block User");
            }
        } else if (i == 2) {
            chatModule.clearUser(chatModule.getChatNode());
            setupChatModule();
        }
    }

    private void setupChatModule() {
        SessionManager sessionManager = new SessionManager(this);
/*
        User user11=new User();
        user11.setId("24");
        user11.setName("User 24");
        sessionManager.setUserDetails("",user11);*/

        User user = sessionManager.getUserDetails("");
        chatModule = new ChatModule(user.getId(), user.getName(), "", this);
        //Commented because of we did a logic change
        //chatModule.readThisGameMsgs();
        chatModule.getMessageList(this);
        //Commentted By Ashvin
        //chatModule.getBlockClearStatus();
    }

    private void setCreateMenu() {
        String blockStatus = chatModule.blockUserStatus();
        Log.e(TAG, "setOptionMenu: $>>" + blockStatus);
        if (blockStatus.equalsIgnoreCase("1")) menu.findItem(R.id.blockUser).setTitle("Unblock User");
        else menu.findItem(R.id.blockUser).setTitle("Block User");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.clear_chat:
                setOptionMenu(2);
                return true;
            case R.id.blockUser:
                setOptionMenu(1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    void audioUploadDialog(String audioPath) {
        DialogInterface.OnClickListener listener = (dialog, which) -> {
            if (which == DialogInterface.BUTTON_POSITIVE) {
                chatModule.uploadAudioMsg(Uri.fromFile(new File(audioPath)));
            } else if (which == DialogInterface.BUTTON_NEGATIVE) {

            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Upload the recorded audio?").setPositiveButton("Yes", listener)
                .setNegativeButton("No", listener).show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (filePicker != null) {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                filePicker.onActivityResult(requestCode, resultCode, data);
                chatModule.uploadImageMsg(filePicker.getImageUri());
            } else if (requestCode == 0 || requestCode == 1) {
                filePicker.onActivityResult(requestCode, resultCode, data);
            } else if (requestCode == 70) {
                filePicker.onActivityResult(requestCode, resultCode, data);
                chatModule.uploadVideoMsg(filePicker.getImageUri());
            }
        }
    }

    @Override
    public void chatMessageListResponse(@NotNull ArrayList<ChatMsgObject> itemList) {
        adapterMessages = new ChatMessagesAdapter(itemList, this, this, this);
        rcvChatMessages.setAdapter(adapterMessages);
        if (!itemList.isEmpty())
            rcvChatMessages.scrollToPosition(itemList.size() - 1);
    }

    @Override
    protected void onPause() {
        super.onPause();
        if(adapterMessages!=null)adapterMessages.pauseSound();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(adapterMessages!=null)adapterMessages.pauseSound();
    }

    @Override
    public void onListSeenClick(int position, @NotNull ChatMsgObject data, @NotNull View view) {

    }
}