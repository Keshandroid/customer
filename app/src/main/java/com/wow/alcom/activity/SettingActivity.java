package com.wow.alcom.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.wow.alcom.BuildConfig;
import com.wow.alcom.R;
import com.wow.alcom.model.User;
import com.wow.alcom.utils.CustPrograssbar;
import com.wow.alcom.utils.SessionManager;
import com.wow.alcom.utils.Utility;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SettingActivity extends BaseActivity {
    SessionManager sessionManager;
    User user;

    @BindView(R.id.txt_name)
    TextView txtName;
    @BindView(R.id.txt_mob)
    TextView txtMob;
    @BindView(R.id.lvl_edit)
    LinearLayout lvlEdit;
    @BindView(R.id.lvl_menu1)
    LinearLayout lvlMenu1;
    @BindView(R.id.lvl_menu7)
    LinearLayout lvl_menu7;
    @BindView(R.id.lvl_menu2)
    LinearLayout lvlMenu2;
    @BindView(R.id.lvl_menu3)
    LinearLayout lvlMenu3;

    @BindView(R.id.lvl_menu4)
    LinearLayout lvlMenu4;
    @BindView(R.id.lvl_menu5)
    LinearLayout lvlMenu5;
    @BindView(R.id.lvl_menu6)
    LinearLayout lvlMenu6;
    @BindView(R.id.lvl_logot)
    LinearLayout lvlLogot;

    @BindView(R.id.lvl_menul4)
    LinearLayout english;


    @BindView(R.id.lvl_menuar4)
    LinearLayout arabic;

    SharedPreferences languagepref,languageprefselect;


    CustPrograssbar custPrograssbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("My Profile");
        custPrograssbar = new CustPrograssbar();
        sessionManager = new SessionManager(SettingActivity.this);
        user = sessionManager.getUserDetails("");

        try {
            if(!user.getName().equals("null"))
            txtName.setText("" + user.getName());
            if(!user.getMobile().equals("null"))
            txtMob.setText("" + user.getMobile());
        }catch (Exception exception){
            exception.printStackTrace();
        }
    }

    @OnClick({R.id.lvl_menu1, R.id.lvl_menu7, R.id.lvl_menu2, R.id.lvl_menu3, R.id.lvl_menu4, R.id.lvl_menu5, R.id.lvl_menu6, R.id.lvl_logot, R.id.lvl_edit, R.id.lvl_contec, R.id.lvl_privacy, R.id.lvl_trams,R.id.lvl_menul4,R.id.lvl_menuar4})
    public void onClick(View view) {
        User ab=new SessionManager(SettingActivity.this).getUserDetails("");
        switch (view.getId()) {
            case R.id.lvl_menu1:

                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=com.wow2all.partner")));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=com.wow.alcom")));
                }
                break;
            case R.id.lvl_menu2:
                if(ab==null || ab.getId()==null || ab.getId().equals("0")){
                    Utility.showDialogForLogin(this);
                }else{
                    startActivity(new Intent(SettingActivity.this, BookingActivity.class));
                }
                break;

            case R.id.lvl_menu3:
                startActivity(new Intent(SettingActivity.this, AboutsActivity.class));

                break;
            case R.id.lvl_contec:
                startActivity(new Intent(SettingActivity.this, ContectusActivity.class));

                break;
            case R.id.lvl_menu4:
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, getResources().getString(R.string.app_name));
                String shareMessage = "Hey! Now use our app to share with your family or friends. User will get wallet amount on your 1st successful order. Enter my referral code *" + 1234 + "* & Enjoy your shopping !!!";
                shareMessage = shareMessage + " https://play.google.com/store/apps/details?id=" + BuildConfig.APPLICATION_ID + "\n\n";
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage);
                startActivity(Intent.createChooser(shareIntent, "choose one"));
                break;
            case R.id.lvl_menu5:
                if(ab==null || ab.getId()==null || ab.getId().equals("0")){
                    Utility.showDialogForLogin(this);
                }else{
                    startActivity(new Intent(SettingActivity.this, ReferlActivity.class));
                }
                break;
            case R.id.lvl_menu6:
                if(ab==null || ab.getId()==null || ab.getId().equals("0")){
                    Utility.showDialogForLogin(this);
                }else{
                    startActivity(new Intent(SettingActivity.this, WalletActivity.class));
                }
                break;
            case R.id.lvl_logot:
                sessionManager.logoutUser();
                Intent intent = new Intent(SettingActivity.this, LoginActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NO_HISTORY);
                startActivity(intent);
                break;
            case R.id.lvl_edit:
                if(ab==null || ab.getId()==null || ab.getId().equals("0")){
                    Utility.showDialogForLogin(this);
                }else{
                    startActivity(new Intent(SettingActivity.this, EditProfileActivity.class));
                }

                break;
            case R.id.lvl_privacy:
                startActivity(new Intent(SettingActivity.this, PrivecyPolicyActivity.class));

                break;
            case R.id.lvl_menu7:
                if(ab==null || ab.getId()==null || ab.getId().equals("0")){
                    Utility.showDialogForLogin(this);
                }else{
                    startActivity(new Intent(SettingActivity.this, ChatMessageActivity.class));
                }

                break;
            case R.id.lvl_trams:
                startActivity(new Intent(SettingActivity.this, TramsAndConditionActivity.class));

                break;

            case R.id.lvl_menul4:
                updateEnglish("en");
                updateLang("ENG");
                break;

            case R.id.lvl_menuar4:
                updateEnglish("ar");
                updateLang("ARB");

                break;


            default:
                break;
        }
    }



    private void updateEnglish(String language)
    {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        languagepref = getSharedPreferences("language",MODE_PRIVATE);
        SharedPreferences.Editor editor = languagepref.edit();
        editor.putString("languageToLoad",language);
        editor.apply();
        editor.commit();

        overridePendingTransition(0, 0);
        startActivity(getIntent());
        overridePendingTransition(0, 0);


    }


    private void updateLang(String langup){
        languageprefselect = getSharedPreferences("lang",MODE_PRIVATE);
        SharedPreferences.Editor editor=languageprefselect.edit();
        editor.putString("ar",langup);
        editor.apply();
        editor.commit();
        startActivity(new Intent(SettingActivity.this, HomeActivity.class));
        finish();


    }




    @Override
    protected void onResume() {
        super.onResume();
        if (sessionManager != null) {
            user = sessionManager.getUserDetails("");
            try {
                txtMob.setText("" + user.getMobile());
                txtName.setText("" + user.getName());
            }catch (Exception e){
                e.printStackTrace();
            }
        }
    }
}