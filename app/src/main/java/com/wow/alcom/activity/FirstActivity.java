package com.wow.alcom.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.os.Bundle;

import androidx.appcompat.app.AppCompatActivity;

import com.wow.alcom.R;
import com.wow.alcom.utils.Identity;
import com.wow.alcom.utils.SessionManager;

import java.util.Locale;

import static com.wow.alcom.utils.SessionManager.login;

public class FirstActivity extends AppCompatActivity {

    SessionManager sessionManager;
    SharedPreferences languageprefselect,languagepref;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_first);

        //show update dialog when re-open the app
        Identity.setUpdateDialog(this, "");

        sessionManager = new SessionManager(FirstActivity.this);
        languageprefselect = getSharedPreferences("lang",MODE_PRIVATE);


        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                } finally {
                    if (sessionManager.getBooleanData(login)) {
                        if(languageprefselect.getString("ar","").equals("ARB")){
                            updateEnglish("ar");

                        }else{
                            updateEnglish("en");

                        }

                        Intent openMainActivity = new Intent(FirstActivity.this, HomeActivity.class);
                        startActivity(openMainActivity);
                        finish();
                    } else {
                        if(languageprefselect.getString("ar","").equals("ARB")){
                            updateEnglish("ar");

                        }else{
                            updateEnglish("en");

                        }

                        Intent openMainActivity = new Intent(FirstActivity.this, IntroActivity.class);
                        startActivity(openMainActivity);
                        finish();

                    }

                }
            }
        };
        timer.start();
    }

    private void updateEnglish(String language)
    {
        Locale locale = new Locale(language);
        Locale.setDefault(locale);
        Configuration config = new Configuration();
        config.locale = locale;
        getBaseContext().getResources().updateConfiguration(config, getBaseContext().getResources().getDisplayMetrics());
        languagepref = getSharedPreferences("language",MODE_PRIVATE);
        SharedPreferences.Editor editor = languagepref.edit();
        editor.putString("languageToLoad",language);
        editor.apply();
        finish();



    }



}