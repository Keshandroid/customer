package com.wow.alcom.activity

import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ImageView
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.google.android.exoplayer2.ExoPlayerFactory
import com.google.android.exoplayer2.SimpleExoPlayer
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory
import com.google.android.exoplayer2.extractor.ExtractorsFactory
import com.google.android.exoplayer2.source.ExtractorMediaSource
import com.google.android.exoplayer2.source.MediaSource
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector
import com.google.android.exoplayer2.trackselection.TrackSelector
import com.google.android.exoplayer2.ui.SimpleExoPlayerView
import com.google.android.exoplayer2.upstream.BandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter
import com.google.android.exoplayer2.upstream.DefaultHttpDataSourceFactory
import com.wow.alcom.R

private const val TAG = "VideoImageActivity"
class VideoImageActivity : BaseActivity() {
    var exoPlayer:SimpleExoPlayer?=null
    var type=""
    var url=""
    var simpleExoPlayer:SimpleExoPlayerView?=null
    var imgView:ImageView?=null
    var backImg:ImageView?=null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_video_image)

        simpleExoPlayer=findViewById(R.id.simpleExoPlayer)
        backImg=findViewById(R.id.backImg)
        imgView=findViewById(R.id.imgView)

        if(intent.hasExtra("type") && intent.getStringExtra("type")!=null){
            type= intent.getStringExtra("type")!!
        }
        if(intent.hasExtra("url") && intent.getStringExtra("url")!=null){
            url= intent.getStringExtra("url")!!
        }

        if(type=="video"){
            simpleExoPlayer!!.visibility= View.VISIBLE
            imgView!!.visibility= View.GONE
            playVideo(simpleExoPlayer!!)
        }else if(type=="image"){
            simpleExoPlayer!!.visibility= View.GONE
            imgView!!.visibility= View.VISIBLE
            Glide.with(this).load(url).error(R.drawable.placeholder1254).placeholder(R.drawable.placeholder1254)
                .into(imgView!!)
        }
        backImg!!.setOnClickListener { onBackPressed() }
    }

    private fun playVideo(videoView: SimpleExoPlayerView){
        Log.d(TAG, "playVideo: testAb>>Opened")
        try {
            val bandwidthMeter: BandwidthMeter = DefaultBandwidthMeter()
            val trackSelector: TrackSelector =
                DefaultTrackSelector(AdaptiveTrackSelection.Factory(bandwidthMeter))
            exoPlayer = ExoPlayerFactory.newSimpleInstance(this, trackSelector)
            val videouri: Uri = Uri.parse(url)
            val dataSourceFactory = DefaultHttpDataSourceFactory("exoplayer_video")
            val extractorsFactory: ExtractorsFactory = DefaultExtractorsFactory()
            val mediaSource: MediaSource =
                ExtractorMediaSource(videouri, dataSourceFactory, extractorsFactory, null, null)
            videoView.setPlayer(exoPlayer)
            exoPlayer!!.prepare(mediaSource)
            exoPlayer!!.playWhenReady = true
        } catch (e: Exception) {
            e.printStackTrace()
            // below line is used for
            // handling our errors.
        }
    }
}