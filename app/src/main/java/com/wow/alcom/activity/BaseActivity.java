package com.wow.alcom.activity;

import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.MenuItem;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.wow.alcom.chatModule.ChatModule;
import com.wow.alcom.model.User;
import com.wow.alcom.utils.SessionManager;

public class BaseActivity extends AppCompatActivity {
    ChatModule chatModule;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState, @Nullable PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @Override
    protected void onResume() {
        super.onResume();
        SessionManager sessionManager=new SessionManager(this);
        User user=sessionManager.getUserDetails("");

        if(chatModule==null && user!=null && user.getId()!=null && user.getName()!=null)
        chatModule=new ChatModule(user.getId(), user.getName(),"", this);

        if(chatModule!=null)
        chatModule.onlineUser();
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onPause() {
        SessionManager sessionManager=new SessionManager(this);
        User user=sessionManager.getUserDetails("");
        if(chatModule==null && user!=null && user.getId()!=null && user.getName()!=null)
            chatModule=new ChatModule(user.getId(), user.getName(), "", this);

        if(chatModule!=null)
        chatModule.offlineUser();

        super.onPause();
    }
}
