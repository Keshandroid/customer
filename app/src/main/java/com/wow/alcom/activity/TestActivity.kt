package com.wow.alcom.activity

import android.os.Bundle
import com.wow.alcom.R
import com.wow.alcom.chatModule.ChatModule
import com.wow.alcom.utils.SessionManager

class TestActivity : BaseActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_test)

        val sessionManager = SessionManager(this)
        val user = sessionManager.getUserDetails("")

        if (chatModule == null && user != null && user.id != null && user.name != null) chatModule =
            ChatModule(user.id, user.name, "", this)

        for(i in 0..1000)
        {
            chatModule.sendStringMessage("I am sending test message $i")
        }
    }
}