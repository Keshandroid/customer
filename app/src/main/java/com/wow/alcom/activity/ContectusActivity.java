package com.wow.alcom.activity;

import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.text.Html;
import android.widget.TextView;


import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.wow.alcom.R;
import com.wow.alcom.adepter.SocialLinksAdapter;
import com.wow.alcom.model.Chiald;
import com.wow.alcom.model.SocialLinkResp;
import com.wow.alcom.retrofit.APIClient;
import com.wow.alcom.retrofit.GetResult;
import com.wow.alcom.utils.CustPrograssbar;
import com.wow.alcom.utils.SessionManager;

import org.json.JSONArray;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;

import static com.wow.alcom.utils.SessionManager.contact;

public class ContectusActivity extends BaseActivity implements GetResult.MyListener {
    @BindView(R.id.txt_contac)
    TextView txtContac;
    SessionManager sessionManager;
    CustPrograssbar custPrograssbar;
    SharedPreferences language;
    RecyclerView rcvSocialLinkList;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contectus);
        ButterKnife.bind(this);
        sessionManager = new SessionManager(this);
        rcvSocialLinkList=findViewById(R.id.rcvSocialLinkList);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            txtContac.setText(Html.fromHtml(sessionManager.getStringData(contact), Html.FROM_HTML_MODE_COMPACT));
        } else {
            txtContac.setText(Html.fromHtml(sessionManager.getStringData(contact)));
        }

        custPrograssbar = new CustPrograssbar();
        language = getSharedPreferences("lang",MODE_PRIVATE);
        getSocialLinks();
    }

    private void getSocialLinks() {
        custPrograssbar.prograssCreate(this);
        JSONObject jsonObject = new JSONObject();

        RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
        Call<JsonObject> call = APIClient.getInterface().getSocialMedia();
        GetResult getResult = new GetResult();
        getResult.setMyListener(this);
        getResult.callForLogin(call, "1");
    }

    @Override
    public void callback(JsonObject result, String callNo) {
        custPrograssbar.closePrograssBar();
        if(callNo.equals("1")){
            Gson gson = new Gson();
            SocialLinkResp socialLinkResp = gson.fromJson(result.toString(), SocialLinkResp.class);
            rcvSocialLinkList.setAdapter(new SocialLinksAdapter(socialLinkResp.getSocialMediaList(),this));
        }
    }
}
