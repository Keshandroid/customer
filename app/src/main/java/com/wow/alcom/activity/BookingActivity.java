package com.wow.alcom.activity;

import android.app.ActivityManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Point;
import android.os.Bundle;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.wow.alcom.R;
import com.wow.alcom.model.CustomerorderlistItem;
import com.wow.alcom.model.Order;
import com.wow.alcom.model.ResponseMessge;
import com.wow.alcom.model.User;
import com.wow.alcom.ooredooCard.PaymentController;
import com.wow.alcom.ooredooCard.listener.IPaymentControllerListener;
import com.wow.alcom.ooredooCard.model.PaymentResp;
import com.wow.alcom.ooredooCard.view.IPaymentView;
import com.wow.alcom.retrofit.APIClient;
import com.wow.alcom.retrofit.GetResult;
import com.wow.alcom.utils.CustPrograssbar;
import com.wow.alcom.utils.SessionManager;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.jetbrains.annotations.NotNull;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;

import static com.wow.alcom.utils.Utility.resedual;


public class BookingActivity extends BaseActivity implements GetResult.MyListener, IPaymentView {


    @BindView(R.id.my_recycler_view)
    RecyclerView myRecyclerView;
    StaggeredGridLayoutManager gridLayoutManager;
    SessionManager sessionManager;
    User user;
    CustPrograssbar custPrograssbar;
    @BindView(R.id.txt_notfount)
    TextView txtNotFount;
    @BindView(R.id.lvl_notfound)
    LinearLayout lvlNotfound;

    ItemAdp itemAdp;
    List<CustomerorderlistItem> orderHistories = new ArrayList<>();
    SharedPreferences languagepref, language;

    private IPaymentControllerListener controller;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_booking);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle(getResources().getString(R.string.menu2));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        controller = new PaymentController(this, this);

        custPrograssbar = new CustPrograssbar();
        sessionManager = new SessionManager(BookingActivity.this);
        user = sessionManager.getUserDetails("");
        gridLayoutManager = new StaggeredGridLayoutManager(1, 1);
        myRecyclerView.setLayoutManager(gridLayoutManager);
        itemAdp = new ItemAdp(BookingActivity.this, new ArrayList<>());
        myRecyclerView.setAdapter(itemAdp);
        language = getSharedPreferences("lang", MODE_PRIVATE);
        getOrder();
    }


    private void getOrder() {
        custPrograssbar.prograssCreate(BookingActivity.this);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uid", user.getId());
            jsonObject.put("lang", language.getString("ar", ""));
        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
        Call<JsonObject> call = APIClient.getInterface().getOrder(bodyRequest);
        GetResult getResult = new GetResult();
        getResult.setMyListener(this);
        getResult.callForLogin(call, "1");

    }

    public void getCancel(String oid, String trackId, String tranId, String amount) {
        custPrograssbar.prograssCreate(BookingActivity.this);

        callAPI(tranId, amount, trackId, oid);
    }

    String oid = "";

    private void callAPI(String transid, String amount, String trackid, String oid) {
        this.oid = oid;
        if (transid == null || transid.equalsIgnoreCase("") || amount == null || amount.equalsIgnoreCase("") || trackid == null || trackid.equalsIgnoreCase("")) {
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("uid", user.getId());
                jsonObject.put("oid", oid);
                RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
                Call<JsonObject> call = APIClient.getInterface().orderCancle(bodyRequest);
                GetResult getResult = new GetResult();
                getResult.setMyListener(this);
                getResult.callForLogin(call, "2");
            } catch (Exception e) {
                e.printStackTrace();

            }
        } else {
            String requestBodyText = "<request>\n" +
                    "<terminalid>WT1</terminalid>\n" +
                    "<password>password</password>\n" +
                    "<action>2</action>\n" +
                    "<currencyCode>QAR</currencyCode>\n" +
                    "<CountryCode>QR</CountryCode>\n" +
                    "<amount>" + amount + "</amount>\n" +
                    "<transid>" + transid + "</transid>\n" +
                    "<trackid>" + trackid + "</trackid>\n" +
                    "</request>";


            controller.callRefundPaymentApi(requestBodyText);
        }

    }

    @Override
    public void callback(JsonObject result, String callNo) {
        try {
            custPrograssbar.closePrograssBar();
            if (callNo.equalsIgnoreCase("1")) {
                Gson gson = new Gson();
                Order orderH = gson.fromJson(result.toString(), Order.class);
                if (orderH.getResult().equalsIgnoreCase("true")) {
                    orderHistories = orderH.getCustomerorderlist();

                    if (!orderHistories.isEmpty()) {
                        lvlNotfound.setVisibility(View.GONE);
                        myRecyclerView.setVisibility(View.VISIBLE);
                        gridLayoutManager = new StaggeredGridLayoutManager(1, 1);
                        myRecyclerView.setLayoutManager(gridLayoutManager);
                        itemAdp = new ItemAdp(BookingActivity.this, orderHistories);
                        myRecyclerView.setAdapter(itemAdp);
                    } else {
                        lvlNotfound.setVisibility(View.VISIBLE);
                        myRecyclerView.setVisibility(View.GONE);
                    }
                } else {
                    lvlNotfound.setVisibility(View.VISIBLE);
                    myRecyclerView.setVisibility(View.GONE);
                }
            } else if (callNo.equalsIgnoreCase("2")) {
                try {
                    /*JSONObject obj = new JSONObject(result.getAsString());
                    String res = obj.getString("Result");
                    String resMsg = obj.getString("ResponseMsg");
                    Toast.makeText(BookingActivity.this, resMsg, Toast.LENGTH_LONG).show();
                    if (res.equalsIgnoreCase("true")) {
                        finish();

                    }*/

                    itemAdp.notifyDataSetChanged();

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    @Override
    public void onSuccess(PaymentResp paymentResp) {
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uid", user.getId());
            jsonObject.put("oid", oid);
            RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
            Call<JsonObject> call = APIClient.getInterface().orderCancle(bodyRequest);
            GetResult getResult = new GetResult();
            getResult.setMyListener(this);
            getResult.callForLogin(call, "2");
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    @Override
    public void onFailed(String message) {

    }

    @Override
    public void showProgress() {

    }

    @Override
    public void dismissProgress() {

    }

    public class ItemAdp extends RecyclerView.Adapter<ItemAdp.ViewHolder> {

        private LayoutInflater mInflater;
        Context mContext;
        List<CustomerorderlistItem> lists;

        public ItemAdp(Context context, List<CustomerorderlistItem> s) {
            this.mInflater = LayoutInflater.from(context);
            this.lists = s;
            this.mContext = context;
            WindowManager wm = (WindowManager) mContext.getSystemService(Context.WINDOW_SERVICE);
            Display display = wm.getDefaultDisplay();
            Point size = new Point();
            display.getSize(size);
        }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View view = mInflater.inflate(R.layout.custome_booking_item, parent, false);
            return new ViewHolder(view);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int i) {
            CustomerorderlistItem item = lists.get(i);
            holder.txtBooking.setText("" + item.getOrderId());
            holder.txtTotal.setText(sessionManager.getStringData(SessionManager.currency) + item.getOrderTotal());
            holder.txtBookingstatus.setText("" + item.getOrderStatus());
            holder.txtBookingdate.setText("" + item.getOrderDateslot() + " " + item.getOrderTime());
            holder.txtAddres.setText("" + item.getCustomerAddress());

            if (item.getOrderStatus().equalsIgnoreCase("Pending")) {
                holder.imgResedul.setVisibility(View.VISIBLE);
                holder.imgCencel.setVisibility(View.VISIBLE);
            } else {
                holder.imgResedul.setVisibility(View.GONE);
                holder.imgCencel.setVisibility(View.GONE);
            }
            holder.imgResedul.setOnClickListener(v -> startActivity(new Intent(BookingActivity.this, ResedulActivity.class)
                    .putExtra("cid", item.getCategoryId())
                    .putExtra("oid", item.getOrderId())));
            holder.imgView.setOnClickListener(v -> startActivity(new Intent(BookingActivity.this, BookingDetailActivity.class)
                    .putExtra("myclass", item)
                    .putParcelableArrayListExtra("addon", item.getAddOnData())
                    .putExtra("itemlist", item.getOrderProductData())));
            holder.imgCencel.setOnClickListener(v -> {

                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(BookingActivity.this);
                alertDialogBuilder.setMessage(getResources().getString(R.string.deletemesseg));
                alertDialogBuilder.setPositiveButton("yes",
                        (arg0, arg1) -> {
                            item.setOrderStatus("Cancel");
                            getCancel(item.getOrderId(), item.getTrackId(), item.getTranId(), item.getAmount());
                        });

                alertDialogBuilder.setNegativeButton("No", (dialog, which) -> finish());
                AlertDialog alertDialog = alertDialogBuilder.create();
                alertDialog.show();

            });
        }

        @Override
        public int getItemCount() {
            return lists.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            @BindView(R.id.txt_booking)
            TextView txtBooking;
            @BindView(R.id.txt_total)
            TextView txtTotal;
            @BindView(R.id.txt_bookingstatus)
            TextView txtBookingstatus;
            @BindView(R.id.txt_bookingdate)
            TextView txtBookingdate;
            @BindView(R.id.txt_addres)
            TextView txtAddres;
            @BindView(R.id.img_view)
            LinearLayout imgView;
            @BindView(R.id.img_resedul)
            LinearLayout imgResedul;
            @BindView(R.id.img_cencel)
            LinearLayout imgCencel;

            ViewHolder(View itemView) {
                super(itemView);
                ButterKnife.bind(this, itemView);
            }

        }


    }

    protected Boolean isActivityRunning(Class activityClass) {
        ActivityManager activityManager = (ActivityManager) getBaseContext().getSystemService(Context.ACTIVITY_SERVICE);
        List<ActivityManager.RunningTaskInfo> tasks = activityManager.getRunningTasks(Integer.MAX_VALUE);

        for (ActivityManager.RunningTaskInfo task : tasks) {
            if (activityClass.getCanonicalName().equalsIgnoreCase(task.baseActivity.getClassName()))
                return true;
        }

        return false;
    }

    @Override
    public boolean onOptionsItemSelected(@NotNull MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            if (!isActivityRunning(HomeActivity.class)) {
                startActivity(new Intent(BookingActivity.this, HomeActivity.class));
                finish();
            } else {
                finish();
            }
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (resedual == 1) {
            resedual = 0;
            getOrder();
        }
    }
}