package com.wow.alcom.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.location.LocationServices;
import com.google.gson.GsonBuilder;
import com.wow.alcom.R;
import com.wow.alcom.adepter.SearchAdapter;
import com.wow.alcom.model.Search;
import com.wow.alcom.model.SearchChildcatdataItem;
import com.wow.alcom.model.User;
import com.wow.alcom.retrofit.APIClient;
import com.wow.alcom.retrofit.GetResult;
import com.wow.alcom.utils.CustPrograssbar;
import com.wow.alcom.utils.SessionManager;
import com.google.gson.Gson;
import com.google.gson.JsonObject;

import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;

public class SearchActivity extends BaseActivity implements GetResult.MyListener, SearchAdapter.RecyclerTouchListener {

    @BindView(R.id.lvl_actionsearch)
    LinearLayout lvlActionsearch;
    @BindView(R.id.ed_search)
    EditText edSearch;
    @BindView(R.id.img_search)
    ImageView imgSearch;
    @BindView(R.id.recycler_product)
    RecyclerView recyclerProduct;
    CustPrograssbar custPrograssbar;
    SessionManager sessionManager;
    User user;
    SharedPreferences languagepref,language;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search);
        ButterKnife.bind(this);
        custPrograssbar = new CustPrograssbar();
        sessionManager = new SessionManager(SearchActivity.this);
        user = sessionManager.getUserDetails("");
        languagepref = getSharedPreferences("language",MODE_PRIVATE);

        language = getSharedPreferences("lang",MODE_PRIVATE);

        recyclerProduct.setLayoutManager(new LinearLayoutManager(this));
        edSearch.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                if (!edSearch.getText().toString().isEmpty()) {
                    getSearch(edSearch.getText().toString());
                }
                return true;
            }
            return false;
        });

    }

    private void getSearch(String s) {

        Log.d("SearchService"," SEARCH KEYWORD : "+ s);


        custPrograssbar.prograssCreate(SearchActivity.this);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject.put("uid", user.getId());
            jsonObject.put("keyword", s);
            jsonObject.put("lang", language.getString("ar",""));

        } catch (Exception e) {
            e.printStackTrace();
        }
        RequestBody bodyRequest = RequestBody.create(MediaType.parse("application/json"), jsonObject.toString());
        Call<JsonObject> call = APIClient.getInterface().getSearch(bodyRequest);
        GetResult getResult = new GetResult();
        getResult.setMyListener(this);
        getResult.callForLogin(call, "1");

    }

    @Override
    public void callback(JsonObject result, String callNo) {

        Log.d("SearchService"," response : " + new GsonBuilder().setPrettyPrinting().create().toJson(result));

        try {
            custPrograssbar.closePrograssBar();
            if (callNo.equalsIgnoreCase("1")) {
                Gson gson = new Gson();
                Search search = gson.fromJson(result.toString(), Search.class);


                SearchAdapter adapter = new SearchAdapter( search.getSearchChildcatdata(), this);
                recyclerProduct.setAdapter(adapter);

            }
        } catch (Exception e) {
            Log.e("Error","-->"+e.toString());
        }
    }

    @Override
    public void onClickSearch(SearchChildcatdataItem dataItem, int position) {

        startActivity(new Intent(this, ItemListActivity.class)
                .putExtra("name", dataItem.getTitle())
                .putExtra("cid", dataItem.getCategoryId())
                .putExtra("sid", dataItem.getSubcatId()));
    }
}