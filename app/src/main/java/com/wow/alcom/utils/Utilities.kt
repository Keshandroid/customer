package com.wow.alcom.utils

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.content.ContentValues
import android.content.Context
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Handler
import android.os.Looper
import android.provider.MediaStore
import android.text.format.DateUtils
import android.util.Log
import android.util.Patterns
import android.view.WindowManager
import android.widget.Toast
import com.wow.alcom.R
import org.apache.http.util.ByteArrayBuffer
import java.io.BufferedInputStream
import java.io.File
import java.net.URL
import java.text.SimpleDateFormat
import java.util.*

@SuppressLint("SetTextI18n", "SimpleDateFormat")
object Utilities {
    val IS_OLD_DATA_ALLOWED = true

    fun getDate(milliSeconds: Long, dateFormat: String?): String {
        return try {
            val formatter = SimpleDateFormat(dateFormat)
            val calendar = Calendar.getInstance()
            calendar.timeInMillis = milliSeconds
            formatter.format(calendar.time)
        } catch (e: Exception) {
            e.printStackTrace()
            ""
        }
    }

    fun compareMilliseconds(otherTime: String): Int {
        try {
            val currTime = Calendar.getInstance().time.time
            val otherTimeLong = otherTime.toLong()
            //gives minutes difference
            val differenceTime = (((currTime - otherTimeLong) / 1000) / 60)
            return differenceTime.toInt()
        } catch (e: java.lang.Exception) {
            e.printStackTrace()
        }
        return -1
    }

    fun getDateTime(): String {
        val date = Date()
        val c = Calendar.getInstance()
        c.time = date
        if (IS_OLD_DATA_ALLOWED)
            c.set(Calendar.DATE, 4)

        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
        return simpleDateFormat.format(c.time).toUpperCase()
    }

    fun getTimeFromStamp(string: String): String {
        val sdf = SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.getDefault())
        val msgDateandTime: String = sdf.format(Date(string.toLong()))

        val dateOfProject: Long = parseDateToddMMyyyy(msgDateandTime)
        val dateOfPresent: Long = System.currentTimeMillis()

        val ab = DateUtils.getRelativeTimeSpanString(
            dateOfProject,
            dateOfPresent,
            DateUtils.MINUTE_IN_MILLIS
        ).toString()
        Log.d(TAG, "getTimeFromStamp: testab71>>$ab>>$dateOfProject>>$dateOfPresent")

        return ab
    }

    fun changeDateFormat(
        strDate: String,
        inputDateFormat: String,
        outputDateFormat: String
    ): String {
        return try {
            SimpleDateFormat(outputDateFormat).format(
                SimpleDateFormat(inputDateFormat).parse(
                    strDate
                )
            )
        } catch (e: Exception) {
            e.printStackTrace()
            Log.d(TAG, "changeDateFormat: error>>$strDate>>$inputDateFormat")
            ""
        }
    }

    fun getDateFromString(strDate: String): Date? {
        return try {
            val format = SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss")
            try {
                return format.parse(strDate)
            } catch (e: Exception) {
                e.printStackTrace()
            }

            return null
        } catch (e: Exception) {
            e.printStackTrace()
            null
        }
    }

    fun getDateDayDiff(strDate: String, strTime: String): Long {
        return try {
            val format = SimpleDateFormat("yyyy-MM-dd HH:mm")
            try {
                val gameDate = format.parse("$strDate $strTime")
                val gameDateMili = gameDate.time
                val diffMili = System.currentTimeMillis() - gameDateMili
                Log.d(TAG, "getDateDayDiff: data>>" + (((diffMili / 1000) / 60) / 60 / 24))
                return (((diffMili / 1000) / 60) / 60 / 24) + 5
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return 0
        } catch (e: Exception) {
            e.printStackTrace()
            0
        }
    }

    private fun parseDateToddMMyyyy(time: String?): Long {
        try {
            val sourceFormat =
                SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            val parsed = sourceFormat.parse(time) // => Date is in Timezone now
            val tz = TimeZone.getTimeZone("UTC")
            val destFormat =
                SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
            destFormat.timeZone = tz
            assert(parsed != null)
            val result = destFormat.format(parsed)
            sourceFormat.timeZone = TimeZone.getTimeZone("UTC")
            val parsedfinal = sourceFormat.parse(result)!!
            return parsedfinal.time
        } catch (ignored: java.lang.Exception) {
        }
        return -1
    }

    public fun getByteArrayImage(url: String?): ByteArray? {
        try {
            val imageUrl = URL(url)
            val ucon = imageUrl.openConnection()
            val `is` = ucon.getInputStream()
            val bis = BufferedInputStream(`is`)
            val baf = ByteArrayBuffer(500)
            var current: Int
            while (bis.read().also { current = it } != -1) {
                baf.append(current)
            }
            return baf.toByteArray()
        } catch (e: Exception) {
            showLog("ImageManager", "Error: $e")
        }
        return null
    }

    private const val TAG = "Utilities"
    fun showLog(string1: String, string2: String) {
        Log.d(TAG, "showLog: >>$string1<<     >>$string2<<")
    }

    fun showLog(string1: String) {
        Log.d(TAG, "showLog: >>$string1")
    }

    fun printKeyHash(activity: Activity) {
        /* try {
             val info: PackageInfo = activity.packageManager.getPackageInfo(
                 "com.ramo",
                 PackageManager.GET_SIGNATURES
             )
             for (signature in info.signatures) {
                 val md: MessageDigest = MessageDigest.getInstance("SHA")
                 md.update(signature.toByteArray())
                 Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT))
             }
         } catch (e: PackageManager.NameNotFoundException) {
         } catch (e: NoSuchAlgorithmException) {
         }*/
    }

    fun isValidEmail(email: String?): Boolean {
        return Patterns.EMAIL_ADDRESS.matcher(email).matches()
    }

    fun getImageContentUri(context: Context, imageFile: File): Uri? {
        val filePath = imageFile.absolutePath
        val cursor = context.contentResolver.query(
            MediaStore.Images.Media.EXTERNAL_CONTENT_URI, arrayOf(MediaStore.Images.Media._ID),
            MediaStore.Images.Media.DATA + "=? ", arrayOf(filePath), null
        )
        return if (cursor != null && cursor.moveToFirst()) {
            val id = cursor.getInt(cursor.getColumnIndex(MediaStore.MediaColumns._ID))
            cursor.close()
            Uri.withAppendedPath(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, "" + id)
        } else {
            if (imageFile.exists()) {
                val values = ContentValues()
                values.put(MediaStore.Images.Media.DATA, filePath)
                context.contentResolver.insert(
                    MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values
                )
            } else {
                null
            }
        }
    }

    var progressDialog: Dialog? = null

    private fun isProgressShowing(): Boolean {
        return if (progressDialog == null)
            false
        else {
            progressDialog!!.isShowing
        }
    }


    var handler: Handler? = null
    fun dismissProgress() {
        if (handler == null) {
            handler = Handler(Looper.getMainLooper())
        } else handler!!.removeCallbacks { }


        handler!!.postDelayed({
            try {
                if (progressDialog != null && progressDialog!!.isShowing) progressDialog!!.dismiss() else Log.i(
                    "Dialog",
                    "already dismissed"
                )
            } catch (e: Exception) {
            }
        }, 2000)


    }

    fun showToast(activity: Activity?, message: String?) {
        try {
            Toast.makeText(activity, message, Toast.LENGTH_SHORT).show()
        } catch (e: Exception) {
        }
    }

}