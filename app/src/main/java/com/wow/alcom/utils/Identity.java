package com.wow.alcom.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

public class Identity {

    private static final String UPDATE_DIALOG = "UPDATE_DIALOG";


    public static String getUpdateDialog(Context mContext) {
        SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        return mPreferences.getString(UPDATE_DIALOG, "");
    }

    public static void setUpdateDialog(Context mContext, String localeKey) {
        SharedPreferences mPreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        mPreferences.edit().putString(UPDATE_DIALOG, localeKey).apply();
    }


}
