package com.wow.alcom.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

public class OrderProductDataItem implements Parcelable {

    @SerializedName("Product_quantity")
    private String productQuantity;

    @SerializedName("Product_price")
    private double productPrice;

    @SerializedName("Product_name")
    private String productName;

    @SerializedName("Product_discount")
    private double productDiscount;

    @SerializedName("Product_total")
    private int productTotal;

    @SerializedName("subcat")
    private String subcat;



    protected OrderProductDataItem(Parcel in) {
        productQuantity = in.readString();
        productPrice = in.readDouble();
        productName = in.readString();
        productDiscount = in.readDouble();
        productTotal = in.readInt();
        subcat = in.readString();
    }

    public static final Creator<OrderProductDataItem> CREATOR = new Creator<OrderProductDataItem>() {
        @Override
        public OrderProductDataItem createFromParcel(Parcel in) {
            return new OrderProductDataItem(in);
        }

        @Override
        public OrderProductDataItem[] newArray(int size) {
            return new OrderProductDataItem[size];
        }
    };

    public String getProductQuantity() {
        return productQuantity;
    }

    public double getProductPrice() {
        return productPrice;
    }

    public String getProductName() {
        return productName;
    }

    public double getProductDiscount() {
        return productDiscount;
    }

    public int getProductTotal() {
        return productTotal;
    }

    public String getSubcat() {
        return subcat;
    }

    public void setSubcat(String subcat) {
        this.subcat = subcat;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(productQuantity);
        dest.writeDouble(productPrice);
        dest.writeString(productName);
        dest.writeDouble(productDiscount);
        dest.writeInt(productTotal);
        dest.writeString(subcat);
    }
}