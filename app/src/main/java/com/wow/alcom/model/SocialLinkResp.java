package com.wow.alcom.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

public class SocialLinkResp {
    @SerializedName("ResponseCode")
    @Expose
    private String responseCode;
    @SerializedName("Result")
    @Expose
    private String result;
    @SerializedName("ResponseMsg")
    @Expose
    private String responseMsg;
    @SerializedName("SocialMediaList")
    @Expose
    private List<SocialMedia> socialMediaList = null;
    private final static long serialVersionUID = 4996133588684416384L;

    public String getResponseCode() {
        return responseCode;
    }

    public void setResponseCode(String responseCode) {
        this.responseCode = responseCode;
    }

    public String getResult() {
        return result;
    }

    public void setResult(String result) {
        this.result = result;
    }

    public String getResponseMsg() {
        return responseMsg;
    }

    public void setResponseMsg(String responseMsg) {
        this.responseMsg = responseMsg;
    }

    public List<SocialMedia> getSocialMediaList() {
        return socialMediaList;
    }

    public void setSocialMediaList(List<SocialMedia> socialMediaList) {
        this.socialMediaList = socialMediaList;
    }

    public class SocialMedia implements Serializable
    {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("social_media_name")
        @Expose
        private String socialMediaName;
        @SerializedName("social_media_link")
        @Expose
        private String socialMediaLink;
        @SerializedName("social_media_logo")
        @Expose
        private String socialMediaLogo;
        @SerializedName("status")
        @Expose
        private String status;
        private final static long serialVersionUID = -8749660797585417847L;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getSocialMediaName() {
            return socialMediaName;
        }

        public void setSocialMediaName(String socialMediaName) {
            this.socialMediaName = socialMediaName;
        }

        public String getSocialMediaLink() {
            return socialMediaLink;
        }

        public void setSocialMediaLink(String socialMediaLink) {
            this.socialMediaLink = socialMediaLink;
        }

        public String getSocialMediaLogo() {
            return socialMediaLogo;
        }

        public void setSocialMediaLogo(String socialMediaLogo) {
            this.socialMediaLogo = socialMediaLogo;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

    }
}
