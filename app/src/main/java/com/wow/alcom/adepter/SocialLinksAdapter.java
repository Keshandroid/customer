package com.wow.alcom.adepter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.Image;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wow.alcom.R;
import com.wow.alcom.model.AddOnDataItem;
import com.wow.alcom.model.SocialLinkResp;
import com.wow.alcom.retrofit.APIClient;
import com.wow.alcom.utils.SessionManager;

import java.util.ArrayList;
import java.util.List;

public class SocialLinksAdapter extends RecyclerView.Adapter<SocialLinksAdapter.MyViewHolder> {
    private Context mContext;
    List<SocialLinkResp.SocialMedia> list;
    Activity activity;
    public SocialLinksAdapter(List<SocialLinkResp.SocialMedia> list, Activity activity){
        this.list=list;
        this.activity=activity;
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public LinearLayout llItem;
        public ImageView img;
        public TextView txt;

        public MyViewHolder(View view) {
            super(view);
            llItem = (LinearLayout) view.findViewById(R.id.llItem);
            img = (ImageView) view.findViewById(R.id.img);
            txt = (TextView) view.findViewById(R.id.txt);
        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_social_link, parent, false));
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {
        Glide.with(activity).load(list.get(position).getSocialMediaLogo()).into(holder.img);
        holder.txt.setText(list.get(position).getSocialMediaName());
        holder.llItem.setOnClickListener(v -> {
            Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(list.get(position).getSocialMediaLink()));
            activity.startActivity(browserIntent);
        });
    }

    @Override
    public int getItemCount() {
        if(list==null) return 0;
        else return list.size();
    }
}