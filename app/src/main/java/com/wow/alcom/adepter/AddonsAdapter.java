package com.wow.alcom.adepter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;
import com.wow.alcom.R;
import com.wow.alcom.model.AddOnDataItem;
import com.wow.alcom.retrofit.APIClient;
import com.wow.alcom.utils.SessionManager;

import java.util.ArrayList;

public class AddonsAdapter extends RecyclerView.Adapter<AddonsAdapter.MyViewHolder> {
    private Context mContext;
    private ArrayList<AddOnDataItem> mCatlist;
    private RecyclerTouchListener listener;
    SessionManager sessionManager;

    public interface RecyclerTouchListener {
        public void onClickAddonsItem(String titel, int position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title,price;
        public ImageView thumbnail ;
        public LinearLayout lvlclick;
        public LinearLayout imgSelect;

        public MyViewHolder(View view) {
            super(view);
            title = (TextView) view.findViewById(R.id.txt_title);
            thumbnail = view.findViewById(R.id.imageView);
            imgSelect = view.findViewById(R.id.img_select);

            price = view.findViewById(R.id.txt_price);
            lvlclick = view.findViewById(R.id.lvl_itemclick);
        }
    }

    public AddonsAdapter(Context mContext, ArrayList<AddOnDataItem> mCatlist, final RecyclerTouchListener listener) {
        this.mContext = mContext;
        this.mCatlist = mCatlist;
        this.listener = listener;
        sessionManager = new SessionManager(mContext);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;
        itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_addone, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, int position) {


        final AddOnDataItem model = mCatlist.get(position);
        Glide.with(mContext).load(APIClient.imageBaseUrl + "/" + model.getImg()).into(holder.thumbnail);

        //original
//        holder.title.setText(model.getTitle()+"");

        //new
        if(model.getTitle() == null){
            holder.title.setText(model.getTitle_a()+""); //arabic language title
        }else {
            holder.title.setText(model.getTitle()+""); // english language title
        }


        holder.price.setText(sessionManager.getStringData(SessionManager.currency)+""+model.getPrice());
        holder.lvlclick.setOnClickListener(v -> {
            model.setSelected(!model.isSelected());
            if (model.isSelected()) {
                holder.imgSelect.setVisibility(View.VISIBLE);
            } else {
                holder.imgSelect.setVisibility(View.GONE);
            }
            listener.onClickAddonsItem("category.getCatname()", position);
        });
    }

    @Override
    public int getItemCount() {
        if(mCatlist==null) return 0;
        else return mCatlist.size();
    }
    public ArrayList<AddOnDataItem> getSelectItem(){
        return  mCatlist;
    }
}