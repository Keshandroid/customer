package com.wow.alcom.adepter

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.media.MediaMetadataRetriever
import android.media.MediaPlayer
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.exoplayer2.SimpleExoPlayer
import com.wow.alcom.R
import com.wow.alcom.activity.VideoImageActivity
import com.wow.alcom.chatModule.objects.ChatMsgObject
import com.wow.alcom.utils.DoAsync
import com.wow.alcom.utils.SessionManager
import com.wow.alcom.utils.Utilities.getTimeFromStamp


private const val TAG = "ChatMessagesAdapter"

class ChatMessagesAdapter() : RecyclerView.Adapter<ChatMessagesAdapter.FreshDailyDealVH>() {
    var arrayList: ArrayList<ChatMsgObject> = ArrayList()
    lateinit var iOnclickListner: IOnclickListner
    lateinit var context: Activity
    lateinit var appContext: Context
    var exoPlayer: SimpleExoPlayer? = null
    //var videoURL = "https://media.geeksforgeeks.org/wp-content/uploads/20201217163353/Screenrecorder-2020-12-17-16-32-03-350.mp4"

    var audioPlayerPos = 0
    var mMediaPlayer: MediaPlayer? = null

    fun playSound(str: String) {


        if (mMediaPlayer != null) {
            mMediaPlayer!!.stop()
            mMediaPlayer!!.release()
            mMediaPlayer = null
        }
        mMediaPlayer = MediaPlayer()
        mMediaPlayer!!.setDataSource(str);
        mMediaPlayer!!.prepare()

        Log.d(TAG, "playSound: testSize>>${mMediaPlayer!!.duration})>>url>>$str")

        mMediaPlayer!!.isLooping = true
        mMediaPlayer!!.start()
    }

    fun pauseSound() {
        if (mMediaPlayer != null && mMediaPlayer!!.isPlaying) mMediaPlayer!!.pause()
    }

    private fun stopSound() {
        if (mMediaPlayer != null) {
            mMediaPlayer!!.stop()
            mMediaPlayer!!.release()
            mMediaPlayer = null
        }
    }

    var userName=""
    var userId=""
    constructor(
        arrayList: ArrayList<ChatMsgObject>,
        onClick: IOnclickListner,
        context: Activity,
        appContext: Context
    ) : this() {
        this.arrayList = arrayList
        iOnclickListner = onClick
        this.context = context
        this.appContext = appContext

        val data=SessionManager(context).getUserDetails("")
        userId=data.id
        if(data.name==null)
        userName="User"
        else userName=data.name
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FreshDailyDealVH {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_chat_messages, parent, false)

        return FreshDailyDealVH(view)
    }

    override fun getItemCount(): Int {
        return arrayList.size
    }
    var tempIV1:ImageView?=null
    var tempIV2:ImageView?=null
    @SuppressLint("UseCompatLoadingForDrawables")
    override fun onBindViewHolder(holder: FreshDailyDealVH, position: Int) {
        val data = arrayList[position]

        val timeString = when {
            getTimeFromStamp(data.messageTime.toString()) == "In 0 minutes" -> "Just now"
            getTimeFromStamp(data.messageTime.toString()) == "0 minutes ago" -> "Just now"
            else -> getTimeFromStamp(data.messageTime.toString())
        }

        if (data.mediaType == "image" || data.mediaType == "video") {
            holder.txtTextMessage.visibility = View.GONE
            holder.imgPlay.visibility = View.GONE
            holder.clVI.visibility = View.VISIBLE
            holder.imgPlay.visibility = View.GONE
            holder.llAudioPlayer.visibility = View.GONE

            if (data.mediaType == "video") {
                holder.imgPlay.visibility = View.VISIBLE
                try {
                    if (data.bitmap != null) Glide.with(context).load(data.bitmap)
                        .error(R.drawable.placeholder1254).placeholder(R.drawable.placeholder1254)
                        .into(holder.imgMessage)
                    else requestHolderLogo(holder, data.messageText, position)
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            } else {
                holder.imgPlay.visibility = View.GONE
                Glide.with(context).load(data.messageText).error(R.drawable.placeholder1254)
                    .placeholder(R.drawable.placeholder1254).into(holder.imgMessage)
            }

        } else if (data.mediaType == "audio") {
            holder.llAudioPlayer.visibility = View.VISIBLE
            holder.clVI.visibility = View.GONE
            holder.txtTextMessage.visibility = View.GONE
            if(audioPlayerPos==position && mMediaPlayer!=null && mMediaPlayer!!.isPlaying){
                holder.imgPlayAudio.visibility = View.GONE
                holder.imgStopAudio.visibility = View.VISIBLE
            }else {
                holder.imgPlayAudio.visibility = View.VISIBLE
                holder.imgStopAudio.visibility = View.GONE
            }
            holder.imgPlayAudio.setOnClickListener {
                if(tempIV1!=null && tempIV2!=null){
                    tempIV1!!.visibility=View.VISIBLE
                    tempIV2!!.visibility=View.GONE
                }

                audioPlayerPos = position
                playSound(data.messageText)
                holder.imgPlayAudio.visibility=View.GONE
                holder.imgStopAudio.visibility=View.VISIBLE

                tempIV1=holder.imgPlayAudio
                tempIV2=holder.imgStopAudio
            }
            holder.imgStopAudio.setOnClickListener {
                audioPlayerPos = position
                stopSound()
                holder.imgPlayAudio.visibility=View.VISIBLE
                holder.imgStopAudio.visibility=View.GONE
            }
        } else {
            holder.llAudioPlayer.visibility = View.GONE
            holder.txtTextMessage.visibility = View.VISIBLE
            holder.clVI.visibility = View.GONE
        }
        holder.txtTextMessage.text = data.messageText



        if(data.senderId==userId){
            holder.txtUsername.text = userName
        }else{
            holder.txtUsername.text = "Admin"
        }

        holder.txtDate.text = timeString

        countSeenStatus(holder.imgSeen, data)

        holder.imgSeen.setOnClickListener {
            iOnclickListner.onListSeenClick(position, arrayList[position], holder.imgSeen)
        }
        holder.imgMessage.setOnClickListener {
            val intent = Intent(context, VideoImageActivity::class.java)
            intent.putExtra("type", data.mediaType)
            intent.putExtra("url", data.messageText)
            context.startActivity(intent)
        }

    }

    private fun countSeenStatus(imageView: ImageView, data: ChatMsgObject) {
        DoAsync {
            try {
                var count1 = 0
                var count2 = 0
                for (i in data.usersList!!.indices) {
                    if (data.usersList!![i].status == 1) {
                        count1 += 1
                    } else if (data.usersList!![i].status == 2) {
                        count2 += 1
                    }
                }
                context.runOnUiThread {
                    when {
                        count1 == data.usersList!!.size -> imageView.setImageDrawable(
                            ContextCompat.getDrawable(context, R.drawable.tick_double_grey)
                        )
                        count2 == data.usersList!!.size -> imageView.setImageDrawable(
                            ContextCompat.getDrawable(context, R.drawable.tick_double_purple)
                        )
                        else -> imageView.setImageDrawable(
                            ContextCompat.getDrawable(
                                context,
                                R.drawable.tick_single_grey
                            )
                        )
                    }
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
    }

    private fun requestHolderLogo(
        holder: FreshDailyDealVH,
        messageText: String,
        position: Int
    ) {
        DoAsync {
            try {
                val retriever = MediaMetadataRetriever()
                retriever.setDataSource(messageText, HashMap())
                val bitmap =
                    retriever.getFrameAtTime(2000, MediaMetadataRetriever.OPTION_CLOSEST_SYNC)

                context.runOnUiThread {
                    arrayList[position].bitmap = bitmap
                    Glide.with((appContext)).load(bitmap).error(R.drawable.placeholder1254).placeholder(R.drawable.placeholder1254).into(holder.imgMessage)
                }
            } catch (e: Exception) {
                e.printStackTrace()
            }
        }
        /**/

    }



    interface IOnclickListner {
        fun onListSeenClick(position: Int, data: ChatMsgObject, view: View)
    }

    override fun onDetachedFromRecyclerView(recyclerView: RecyclerView) {
        super.onDetachedFromRecyclerView(recyclerView)

    }

    inner class FreshDailyDealVH(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val clVI: ConstraintLayout = itemView.findViewById(R.id.clVI)

        val llAudioPlayer: LinearLayout = itemView.findViewById(R.id.llAudioPlayer)
        val imgPlayAudio: ImageView = itemView.findViewById(R.id.imgPlayAudio)
        val imgStopAudio: ImageView = itemView.findViewById(R.id.imgStopAudio)
        val txtUsername: TextView = itemView.findViewById(R.id.txtUsername)
        val txtTextMessage: TextView = itemView.findViewById(R.id.txtTextMessage)
        val imgSeen: ImageView = itemView.findViewById(R.id.imgSeen)
        val txtDate: TextView = itemView.findViewById(R.id.txtDate)

        val imgMessage: ImageView = itemView.findViewById(R.id.imgMessage)
        val imgPlay: ImageView = itemView.findViewById(R.id.imgPlay)
    }

}