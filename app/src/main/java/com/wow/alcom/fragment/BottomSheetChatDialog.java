package com.wow.alcom.fragment;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.bottomsheet.BottomSheetDialogFragment;
import com.theartofdev.edmodo.cropper.CropImage;
import com.wow.alcom.R;
import com.wow.alcom.adepter.ChatMessagesAdapter;
import com.wow.alcom.chatModule.AudioRecorder;
import com.wow.alcom.chatModule.ChatModule;
import com.wow.alcom.chatModule.FilePicker;
import com.wow.alcom.chatModule.objects.ChatMsgObject;
import com.wow.alcom.model.User;
import com.wow.alcom.utils.SessionManager;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;

import hani.momanii.supernova_emoji_library.Actions.EmojIconActions;

public class BottomSheetChatDialog extends BottomSheetDialogFragment implements ChatModule.ChatMessageListResponse, ChatMessagesAdapter.IOnclickListner {

    Activity activity;

    public BottomSheetChatDialog(Activity activity) {
        this.activity = activity;
    }

    FilePicker filePicker;
    ChatModule chatModule;
    AudioRecorder audioRecord;
    ChatMessagesAdapter adapterMessages;

    LinearLayout llMain;
    ImageView imgEmojiKB;
    RecyclerView rcvChatMessages;
    ImageView imgSend, imgPick, imgAudioRecord;
    hani.momanii.supernova_emoji_library.Helper.EmojiconEditText etxtMessage;
    String[] title = {
            "Video",
            "Image",
            "Cancel"
    };
    View v;

    @Override
    public void onCreate(@Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setStyle(BottomSheetDialogFragment.STYLE_NORMAL, R.style.CustomBottomSheetDialogTheme);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable
            ViewGroup container, @Nullable Bundle savedInstanceState) {
        v = inflater.inflate(R.layout.bottomsheet_chat, container, false);
        return v;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable @org.jetbrains.annotations.Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (filePicker != null) {
            if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
                filePicker.onActivityResult(requestCode, resultCode, data);
                chatModule.uploadImageMsg(filePicker.getImageUri());
            } else if (requestCode == 0 || requestCode == 1) {
                filePicker.onActivityResult(requestCode, resultCode, data);
            } else if (requestCode == 70) {
                filePicker.onActivityResult(requestCode, resultCode, data);
                chatModule.uploadVideoMsg(filePicker.getImageUri());
            }
        }
    }

    @Override
    public void onViewCreated(@NonNull @NotNull View view, @Nullable @org.jetbrains.annotations.Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        SessionManager sm = new SessionManager(activity);
        User user = sm.getUserDetails("");

        LinearLayout llMain = v.findViewById(R.id.llMain);
        TextView txtName = v.findViewById(R.id.txtName);
        llMain.setOnClickListener(v1 -> dismiss());
        if (user == null || user.getName() == null)
            txtName.setText("Hi User");
        else txtName.setText("Hi " + user.getName());
        imgEmojiKB = v.findViewById(R.id.imgEmojiKB);
        rcvChatMessages = v.findViewById(R.id.rcvChatMessages);
        imgAudioRecord = v.findViewById(R.id.imgAudioRecord);
        etxtMessage = v.findViewById(R.id.etxtMessage);
        imgPick = v.findViewById(R.id.imgPick);
        imgSend = v.findViewById(R.id.imgSend);
        audioRecord = new AudioRecorder(activity);

        EmojIconActions emojIcon = new EmojIconActions(activity, llMain, etxtMessage, imgEmojiKB, "#2c003e", "#CFCFCF", "#E1DFDF");
        emojIcon.ShowEmojIcon();
        etxtMessage.setUseSystemDefault(true);

        imgSend.setOnClickListener(v -> {
            if (etxtMessage.getText().toString().trim().equalsIgnoreCase("")) {
                Toast.makeText(activity, "Please enter message to continue.", Toast.LENGTH_LONG).show();
            } else {
                chatModule.sendStringMessage(etxtMessage.getText().toString().trim());
                etxtMessage.setText("");
            }
        });
        imgAudioRecord.setOnTouchListener((v, event) -> {

            if (event.getAction() == MotionEvent.ACTION_DOWN) {
                audioRecord.startRecording();
            } else if (event.getAction() == MotionEvent.ACTION_UP) {
                audioRecord.stopRecording();
                audioUploadDialog(audioRecord.getAudioPath());
            }

            return true;
        });
        imgPick.setOnClickListener(v -> {
            String[] options = {"Video", "Image", "Cancel"};
            AlertDialog.Builder builder = new AlertDialog.Builder(activity);
            builder.setItems(options, (dialog, which) -> {
                if (title[which].equalsIgnoreCase("Video")) {
                    filePicker = new FilePicker(activity, 2);
                } else if (title[which].equalsIgnoreCase("Image")) {
                    filePicker = new FilePicker(activity, 1);
                } else if (title[which].equalsIgnoreCase("Cancel")) {
                    dialog.dismiss();
                }
            });
            builder.show();
        });

        setupChatModule();
    }

    void audioUploadDialog(String audioPath) {
        DialogInterface.OnClickListener listener = (dialog, which) -> {
            if (which == DialogInterface.BUTTON_POSITIVE) {
                chatModule.uploadAudioMsg(Uri.fromFile(new File(audioPath)));
            } else if (which == DialogInterface.BUTTON_NEGATIVE) {

            }
        };

        AlertDialog.Builder builder = new AlertDialog.Builder(activity);
        builder.setMessage("Upload the recorded audio?").setPositiveButton("Yes", listener)
                .setNegativeButton("No", listener).show();
    }

    @Override
    public void onDismiss(@NonNull @NotNull DialogInterface dialog) {
        super.onDismiss(dialog);
        if (adapterMessages != null)
            adapterMessages.pauseSound();
    }

    @Override
    public void onPause() {
        super.onPause();

        if (adapterMessages != null)
            adapterMessages.pauseSound();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (adapterMessages != null)
            adapterMessages.pauseSound();
    }

    private void setupChatModule() {
        SessionManager sessionManager = new SessionManager(activity);
        User user = sessionManager.getUserDetails("");
        if (user == null || user.getId() == null || user.getName() == null) {

        } else {
            chatModule = new ChatModule(user.getId(), user.getName(), "", activity);
            chatModule.readThisGameMsgs();
            chatModule.getBlockClearStatus();
            chatModule.getMessageList(this);
        }
    }

    @Override
    public void chatMessageListResponse(@NotNull ArrayList<ChatMsgObject> itemList) {
        adapterMessages = new ChatMessagesAdapter(itemList, this, activity, activity);
        rcvChatMessages.setAdapter(adapterMessages);
        if (!itemList.isEmpty())
            rcvChatMessages.scrollToPosition(itemList.size() - 1);
    }

    @Override
    public void onListSeenClick(int position, @NotNull ChatMsgObject data, @NotNull View view) {

    }
}