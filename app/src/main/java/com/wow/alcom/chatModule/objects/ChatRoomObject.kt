package com.wow.alcom.chatModule.objects

class ChatRoomObject {
    var userName: String = ""
    var userImage: String = ""

    var messageTime: Long = 0
    var mediaName: String = ""

    var mediaType: String = ""
    var messageText: String = ""

    var senderId: String = ""

    var count: String = ""
}