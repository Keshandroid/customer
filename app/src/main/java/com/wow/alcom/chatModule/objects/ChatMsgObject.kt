package com.wow.alcom.chatModule.objects

import android.graphics.Bitmap
import com.wow.alcom.chatModule.objects.ChatMsgMembersObject

class ChatMsgObject {
    var mediaName: String = ""
    var mediaType: String = ""
    var senderId: String = ""
    var messageTime: Long = 0
    var messageText: String = ""
    var usersList:ArrayList<ChatMsgMembersObject>?=null

    /*not from FB*/
    var userName: String = ""

    var bitmap: Bitmap? = null
}
