package com.wow.alcom.chatModule

import android.app.Activity
import android.app.ProgressDialog
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.widget.Toast
import com.google.firebase.database.*
import com.google.firebase.storage.FirebaseStorage
import com.google.firebase.storage.StorageReference
import com.wow.alcom.chatModule.objects.*
import java.io.*
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.MutableMap
import kotlin.collections.indices
import kotlin.collections.set

const val strKey: String = "COUNT_LEAGUE_LIST"

class ChatModule(
    val currentuserId: String,
    val currentuserName: String?,
    val membersList: String,
    val activity: Activity
) {
    object BlockUser {
        var STATUS = "status"
    }

    private val DB_NODE_USER = "users"
    private val DB_NODE_MESSAGES = "messages"
    private val DB_NODE_CHATS = "chats"


    private var mDatabase: DatabaseReference? = null
    private var mStorage: StorageReference? = null

    private var bitmapdata: ByteArray? = null
    private var gameId = ""

    val blockMsg1="You have blocked this user."
    val blockMsg2="You are blocked by admin."
    var currUserBlockStatus="0"
    var otherUserBlockStatus="0"
    var currUserLastMsg:Long=0
    var otherUserLastMsg:Long=0

    init {
        gameId = "000-$currentuserId"

        try {
            FirebaseDatabase.getInstance().setPersistenceEnabled(true)
        } catch (e: Exception) {
        }
        mDatabase = FirebaseDatabase.getInstance().reference

        val recentItem = mDatabase!!.child(DB_NODE_USER).child(currentuserId)
        val value1: MutableMap<String, Any> = HashMap()
        value1[ChatUsersObject.ONLINE] = "1"
        recentItem.updateChildren(value1)
    }

    fun sendStringMessage(msg: String) {
        if (currUserBlockStatus == "1") {
            Toast.makeText(activity, blockMsg1, Toast.LENGTH_LONG).show()
            return@sendStringMessage
        }
        if (otherUserBlockStatus == "1") {
            Toast.makeText(activity, blockMsg2, Toast.LENGTH_LONG).show()
            return@sendStringMessage
        }

        val timestamp = System.currentTimeMillis()
        val chatItem = mDatabase!!.child(DB_NODE_MESSAGES).child(gameId).push()




        val value: MutableMap<String, Any> = HashMap()
        value[ChatObject.MESSAGE_TEXT] = msg
        value[ChatObject.MESSAGE_TIME] = timestamp
        value[ChatObject.SENDER_ID] = currentuserId
        value[ChatObject.MEDIA_NAME] = ""
        value[ChatObject.MEDIA_TYPE] = "text"

        val list = membersList.split(",")
        val listArray = ArrayList<ChatMsgMembersObject>()

        for (i in list.indices) {
            val chatMsgMembersObject = ChatMsgMembersObject()
            chatMsgMembersObject.status = 0
            //chatMsgMembersObject.userId = list[i]
            listArray.add(chatMsgMembersObject)
        }
        value[ChatObject.USERS_LIST] = listArray

        chatItem.setValue(value)




        //store message data in "chats"
        val value2: MutableMap<String, Any> = HashMap()
        value2[ChatObject.MESSAGE_TEXT] = msg
        value2[ChatObject.MESSAGE_TIME] = timestamp
        value2[ChatObject.SENDER_ID] = currentuserId
        value2[ChatObject.MEDIA_NAME] = ""
        value2[ChatObject.MEDIA_TYPE] = "text"
        value2["count"] = "0"

        value2["userImage1"] = ""
        value2["userImage2"] = ""
        value2["userName1"] = ""
        value2["userName2"] = ""


        mDatabase!!.child(DB_NODE_CHATS).child(gameId).setValue(value2)
    }

    private val TAG = "ChatModule"
    fun getMessageList(chatListResponse: ChatMessageListResponse) {

        Log.d(TAG, "getMessageList() called with gameId : ${gameId}")
        val newRef = mDatabase!!.child("messages").child(gameId)
        newRef.keepSynced(true)
        val arrayList: ArrayList<ChatMsgObject> = ArrayList()

        newRef.get().addOnCompleteListener {
            Log.e(TAG, "getMessageList: I am here....${it}");
            if(it.isSuccessful)
            {
                var snapshot = it.result
                Log.e(TAG, "getMessageList: I am here Size : ${snapshot.childrenCount}" )
                arrayList.clear()
                try {
                    for (postSnapshot in snapshot.children) {
                        val data: ChatMsgObject =
                                postSnapshot.getValue(ChatMsgObject::class.java)!!

                        if (data.messageTime > currUserLastMsg) {

                            Log.d(TAG, "onDataChange: testAb>>Top>>"+data.messageTime+">>"+currUserLastMsg)
                            arrayList.add(data)
                        }


                        blahUpdateData(data,newRef,postSnapshot)
                    }
                } catch (e: Exception) {
                    Log.d("ChatError", "Exception : "+e.localizedMessage)

                    e.printStackTrace()
                }
                chatListResponse.chatMessageListResponse(arrayList)
            }

            var tempList = ArrayList<ChatMsgObject>()
            newRef.addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    tempList.clear()
                    try {
                        for (postSnapshot in snapshot.children) {
                            val data: ChatMsgObject =
                                    postSnapshot.getValue(ChatMsgObject::class.java)!!

                            //if (data.messageTime > currUserLastMsg) {

                                tempList.add(data)
                            //}
                        }
                    } catch (e: Exception) {
                        Log.d("ChatError", "Exception : "+e.localizedMessage)

                        e.printStackTrace()
                    }
                    chatListResponse.chatMessageListResponse(tempList)
                }

                override fun onCancelled(databaseError: DatabaseError) {

                }
            })
        }

    }

    private fun blahUpdateData(data: ChatMsgObject, databaseRef: DatabaseReference, postSnapshot: DataSnapshot) {
        for (i in data.usersList!!.indices) {
            Log.d(TAG, "onDataChange: testData>>Item>>"+i+">>"+data.senderId)
            if ((data.senderId == "000"||data.senderId == "0") && data.usersList!![i].status != 2) {

                val newRef =
                        databaseRef.ref.child(postSnapshot.key.toString()).child("usersList")
                                .child(i.toString())
                val value: MutableMap<String, Any> = HashMap()
                value["status"] = 2

                newRef.updateChildren(value)

                Log.d(TAG, "onDataChange: testSize>>Item>>$newRef")
            }
        }
    }

    fun getBlockClearStatus() {
        val newRef1 = mDatabase!!.child("blockClear").child(gameId).child(currentuserId).child("status")
        newRef1.keepSynced(true)
        newRef1.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                try {
                    val status=snapshot.getValue(String::class.java)
                    Log.d(TAG, "getBlockClearStatus onDataChange:currUserBlockStatus:$currentuserId>> testAb>>$status")

                    if(status!=null && status!="" && status!="null"){
                        currUserBlockStatus = status!!
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {
            }
        })
        val newRef2 = mDatabase!!.child("blockClear").child(gameId).child("000").child("status")
        newRef2.keepSynced(true)
        newRef2.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                try {
                    val status=snapshot.getValue(String::class.java)
                    Log.d(TAG, "getBlockClearStatus onDataChange:000>> testAb>>$status")

                    if(status!=null && status!="" && status!="null"){
                        otherUserBlockStatus = status
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {
            }
        })
        val newRef3 = mDatabase!!.child("blockClear").child(gameId).child("userLastMsg2")
        newRef3.keepSynced(true)
        newRef3.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                try {
                    val status=snapshot.getValue(Long::class.java)
                    Log.d(TAG, "getBlockClearStatus onDataChange:userLastMsg1>> testAb>>$status")

                    Log.d(TAG, "onDataChange: testAb>>1>"+status)
                    if(status!=null){
                        Log.d(TAG, "onDataChange: testAb>>"+status)
                        currUserLastMsg = status
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {
            }
        })
        val newRef = mDatabase!!.child("blockClear").child(gameId).child("userLastMsg1")
        newRef.keepSynced(true)
        newRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                try {
                    val status=snapshot.getValue(Long::class.java)
                    Log.d(TAG, "getBlockClearStatus onDataChange:userLastMsg2>> testAb>>$status")

                    if(status!=null){
                        otherUserLastMsg = status
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
            }
            override fun onCancelled(databaseError: DatabaseError) {
            }
        })
    }

    fun getMembersList(resp: ChatMembersListResponse) {
        val msmList = membersList.split(",")

        val newRef = mDatabase!!.child("users")
        newRef.keepSynced(true)
        val arrayList: ArrayList<ChatMsgMembersObject> = ArrayList()
        newRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                arrayList.clear()

                for (postSnapshot in snapshot.children) {
                    try {
                        var flag = false
                        for (i in msmList.indices) {
                            if (postSnapshot.key.toString() == msmList[i]) {
                                flag = true
                                Log.d(TAG, "onDataChange: test124>>>" + postSnapshot.key.toString())
                            }
                        }
                        if (flag) {
                            val item = ChatMsgMembersObject()
                            //item.userId = postSnapshot.key.toString()
                            Log.d(TAG, "onDataChange: test 129>>++--")
                            val data: MembersTempObject =
                                postSnapshot.getValue(MembersTempObject::class.java)!!
                            item.status = data.online

                            Log.d(TAG, "onDataChange: test124>>>" + ">>" + data.online)

                            arrayList.add(item)
                        }
                    } catch (e: Exception) {
                        e.printStackTrace()
                        Log.d(TAG, "onDataChange: test124>>> onDataChange: test>>" + e.message)
                    }
                }

                resp.chatMembersListResponse(arrayList)
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })
    }

    fun offlineUser() {
        val recentItem = mDatabase!!.child(DB_NODE_USER).child(currentuserId)
        val value1: MutableMap<String, Any> = HashMap()
        value1[ChatUsersObject.ONLINE] = "0"
        recentItem.updateChildren(value1)
    }

    fun onlineUser() {

        val recentItem = mDatabase!!.child(DB_NODE_USER).child(currentuserId)
        val value1: MutableMap<String, Any> = HashMap()
        value1[ChatUsersObject.ONLINE] = "1"
        recentItem.updateChildren(value1)

        val newRef = mDatabase!!.child("messages")
        newRef.keepSynced(true)
        newRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                try {
                    for (postSnapshot in snapshot.children) {
                        val data = postSnapshot.key
                        Log.d(TAG, "onlineUser onDataChange: test>>$data")
                        getDataByGameId(data!!, false,true)
                    }
                } catch (e: Exception) {
                    Log.d(TAG, "onlineUser onDataChange: test>>" + e.message)
                }
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })
    }

    private fun getDataByGameId(groupId: String, isReadMsg: Boolean, removeListner: Boolean) {
        Log.d(TAG, "onDataChange: testData>>getDataByGameId>>"+groupId)

        val newRef = mDatabase!!.child("messages").child(groupId)
        newRef.keepSynced(true)
        newRef.addValueEventListener(object : ValueEventListener {
            override fun onDataChange(snapshot: DataSnapshot) {
                try {
                    for (postSnapshot in snapshot.children) {
                        val data: ChatMsgTempObject =
                            postSnapshot.getValue(ChatMsgTempObject::class.java)!!

                        Log.d(TAG, "onDataChange: testData>>senderId>>"+data.senderId+">>currUserId>>"+currentuserId+">>listSize>>"+data.usersList!!.size+">>isReadMsg>>"+isReadMsg)
                        for (i in data.usersList!!.indices) {
                            Log.d(TAG, "onDataChange: testData>>Item>>"+i+">>"+data.senderId)
                            if ((data.senderId == "000"||data.senderId == "0") && data.usersList!![i].status != 2) {

                                val newRef =
                                    newRef.ref.child(postSnapshot.key.toString()).child("usersList")
                                        .child(i.toString())
                                val value: MutableMap<String, Any> = HashMap()
                                if (isReadMsg) value["status"] = 2
                                else {
                                    value["status"] = 1
                                }
                                newRef.updateChildren(value)

                                Log.d(TAG, "onDataChange: testSize>>Item>>$newRef")
                            }
                        }
                    }
                } catch (e: Exception) {
                    e.printStackTrace()
                }
                if(removeListner) newRef.removeEventListener(this)
            }

            override fun onCancelled(databaseError: DatabaseError) {

            }
        })
    }

    fun readThisGameMsgs() {
        getDataByGameId(gameId, true,false)
    }

    fun uploadAudioMsg(audioUri: Uri?) {
        if (currUserBlockStatus == "1") {
            Toast.makeText(activity, blockMsg1, Toast.LENGTH_LONG).show()
            return@uploadAudioMsg
        }
        if (otherUserBlockStatus == "1") {
            Toast.makeText(activity, blockMsg2, Toast.LENGTH_LONG).show()
            return@uploadAudioMsg
        }

        mStorage = FirebaseStorage.getInstance().reference
        /*Audio*/
        if (audioUri != null) {
            uploadMedia(audioUri, 2)
        } else Toast.makeText(activity, "Audio upload failed.", Toast.LENGTH_LONG).show()
    }

    fun uploadImageMsg(imageUri: Uri?) {
        if (currUserBlockStatus == "1") {
            Toast.makeText(activity, blockMsg1, Toast.LENGTH_LONG).show()
            return@uploadImageMsg
        }
        if (otherUserBlockStatus == "1") {
            Toast.makeText(activity, blockMsg2, Toast.LENGTH_LONG).show()
            return@uploadImageMsg
        }

        mStorage = FirebaseStorage.getInstance().reference
        /*Image*/
        if (imageUri != null) {
            val contentURI: Uri = imageUri
            var filedata: File? = null
            val projection =
                arrayOf(MediaStore.Images.Media.DATA)
            val cursor: Cursor? =
                activity.contentResolver.query(contentURI, projection, null, null, null)
            if (cursor != null) {
                cursor.moveToFirst()
                cursor.close()
            }

            try {
                val thumbnail = MediaStore.Images.Media.getBitmap(
                    activity.contentResolver,
                    contentURI
                )
                val imageByte1: ByteArray? = getFileDataFromDrawable(thumbnail)
                var byteArray: ByteArray? = null
                try {
                    val bitmap =
                        BitmapFactory.decodeByteArray(imageByte1, 0, imageByte1!!.size)
                    val blob =
                        ByteArrayOutputStream()
                    bitmap.compress(Bitmap.CompressFormat.PNG, 20, blob)
                    byteArray = blob.toByteArray()

                    try {
                        val file = File(
                            Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_DOWNLOADS
                            ), "share_image_" + System.currentTimeMillis() + ".png"
                        )
                        file.parentFile.mkdirs()
                        val out = FileOutputStream(file)
                        bitmap.compress(Bitmap.CompressFormat.PNG, 90, out)
                        out.close()
                        filedata = file
                        bitmapdata

                    } catch (e: IOException) {
                        e.printStackTrace()
                    }
                } catch (e: java.lang.Exception) {
                }
                if (byteArray == null) {
                    Toast.makeText(
                        activity,
                        "Unable to get image. Try again later.",
                        Toast.LENGTH_LONG
                    ).show()
                } else {
                    var path = ""
                    path = filedata!!.path
                    Log.e(TAG, "uploadImageMsg: $path")
                    bitmapdata = byteArray
                    uploadMedia(Uri.fromFile(File(path)), 0)
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }

        } else Toast.makeText(activity, "Image selection failed.", Toast.LENGTH_LONG).show()
    }

    private fun uploadMedia(uri: Uri, isImage: Int) {
        val filePath = uri.path

        if (filePath != null) {
            val progressDialog = ProgressDialog(activity)
            progressDialog.setTitle("Uploading...")
            progressDialog.show()

            val fileName = System.currentTimeMillis().toString() + "_" + currentuserId + "_" + File(
                uri.path
            ).name /*+"."+fileType*/
            val ref: StorageReference = when (isImage) {
                0 -> {
                    mStorage!!
                        .child("images" + File.separator + fileName)
                }
                1 -> {
                    mStorage!!
                        .child("videos" + File.separator + fileName)
                }
                2 -> {
                    mStorage!!
                        .child("audios" + File.separator + fileName)
                }
                else -> null
            } ?: return

            Log.d(TAG, "uploadMedia: 334>>$ref")

            val uploadTask = ref.putFile(uri)
            uploadTask.addOnProgressListener { taskSnapshot ->
                val progress = (100.0
                        * taskSnapshot.bytesTransferred
                        / taskSnapshot.totalByteCount)
                progressDialog.setMessage(
                    "Uploaded "
                            + progress.toInt() + "%"
                )
            }
            uploadTask.continueWithTask { task ->
                if (!task.isSuccessful) {
                    Log.d(TAG, "uploadMedia: testAb248>>" + task.exception)
                    throw task.exception!!
                }
                ref.downloadUrl
            }.addOnCompleteListener { task ->
                Log.d(
                    TAG,
                    "uploadMedia: testAb248>>" + task.exception + ">>" + task.isComplete + ">>"
                )
                if (task.isSuccessful) {
                    val downloadUri = task.result
                    Log.e(TAG, "download url: $downloadUri")
                    progressDialog.dismiss()
                    Toast.makeText(activity, "Media Uploaded!!", Toast.LENGTH_LONG).show()

                    val timestamp = System.currentTimeMillis()
                    val chatItem = mDatabase!!.child(DB_NODE_MESSAGES).child(gameId).push()
                    val value: MutableMap<String, Any> = HashMap()
                    value[ChatObject.MESSAGE_TEXT] = downloadUri.toString()
                    value[ChatObject.MESSAGE_TIME] = timestamp
                    value[ChatObject.SENDER_ID] = currentuserId
                    value[ChatObject.MEDIA_NAME] = ""

                    if (isImage == 0) value[ChatObject.MEDIA_TYPE] = "image"
                    else if (isImage == 1) value[ChatObject.MEDIA_TYPE] = "video"
                    else if (isImage == 2) value[ChatObject.MEDIA_TYPE] = "audio"

                    val list = membersList.split(",")
                    val listArray = ArrayList<ChatMsgMembersObject>()
                    for (i in list.indices) {
                        val chatMsgMembersObject = ChatMsgMembersObject()
                        chatMsgMembersObject.status = 0
                        //chatMsgMembersObject.userId = list[i]
                        listArray.add(chatMsgMembersObject)
                    }
                    value[ChatObject.USERS_LIST] = listArray

                    chatItem.setValue(value)
                } else {
                    progressDialog.dismiss()
                    Toast.makeText(activity, "Erro uploading file.", Toast.LENGTH_LONG).show()
                }
            }
        }
    }

    private fun getFileDataFromDrawable(bitmap: Bitmap): ByteArray? {
        return try {
            val byteArrayOutputStream =
                ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream)
            byteArrayOutputStream.toByteArray()
        } catch (e: java.lang.Exception) {
            null
        }
    }

    fun uploadVideoMsg(imageUri: Uri?) {
        if (currUserBlockStatus == "1") {
            Toast.makeText(activity, blockMsg1, Toast.LENGTH_LONG).show()
            return@uploadVideoMsg
        }
        if (otherUserBlockStatus == "1") {
            Toast.makeText(activity, blockMsg2, Toast.LENGTH_LONG).show()
            return@uploadVideoMsg
        }

        mStorage = FirebaseStorage.getInstance().reference
        /*Video*/
        if (imageUri != null) {
            uploadMedia(imageUri, 1)
        } else Toast.makeText(activity, "Video selection failed.", Toast.LENGTH_LONG).show()
    }

    fun getChatNode(): String {
        return gameId
    }

    fun blockUser(nodeList: String) {
        Log.d(TAG, "blockUser: >>$nodeList")
        val value: MutableMap<String, Any> = HashMap()
        value[BlockUser.STATUS] = "1"
        mDatabase!!.child("blockClear").child(nodeList).child(currentuserId).setValue(value)
    }

    fun unblockUser(nodeList: String) {
        Log.d(TAG, "blockUser: >>$nodeList")
        val value: MutableMap<String, Any> = HashMap()
        value[BlockUser.STATUS] = "0"
        mDatabase!!.child("blockClear").child(nodeList).child(currentuserId).setValue(value)
    }

    fun blockUserStatus(): String {
        return currUserBlockStatus
    }

    fun clearUser(nodeList: String) {
        Log.d(TAG, "clearUser: >>$nodeList")
        val value1: MutableMap<String, Any> = HashMap()

        if (getNodePart(nodeList, 0) == currentuserId)
            value1["userLastMsg1"] = ServerValue.TIMESTAMP
        else if (getNodePart(nodeList, 1) == currentuserId)
            value1["userLastMsg2"] = ServerValue.TIMESTAMP

        mDatabase!!.child("blockClear").child(nodeList).updateChildren(value1)
        //mDatabase.child("messages").child(nodeList[i]).removeValue()
    }

    fun getNodePart(str: String, int: Int): String {
        return try {
            str.split("-")[int]
        } catch (e: Exception) {
            ""
        }
    }

    interface ChatMessageListResponse {
        fun chatMessageListResponse(itemList: ArrayList<ChatMsgObject>)
    }

    interface BlockClearStatus {
        fun onResponse(blockCurrentUser:String,blockOtherUser:String,lastMsgCurrentUser:String,lastMsgOtherUser:String)
    }

    interface ChatMembersListResponse {
        fun chatMembersListResponse(itemList: ArrayList<ChatMsgMembersObject>)
    }
}