package com.wow.alcom.chatModule.objects

object ChatObject {
    var MEDIA_NAME = "mediaName"
    var MEDIA_TYPE = "mediaType"
    var SENDER_ID = "senderId"
    var MESSAGE_TIME = "messageTime"
    var MESSAGE_TEXT = "messageText"
    var USERS_LIST = "usersList"
}